package tobiass.rooster;

import java.util.Iterator;

import tobiass.rooster.api.Friend;
import tobiass.rooster.api.School;
import tobiass.rooster.background.NotificationService;
import tobiass.rooster.background.NotificationService.ServiceBinder;
import tobiass.rooster.misc.Const;
import tobiass.rooster.storage.Friends;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class TestDrive extends SherlockActivity {
	private TextView t = null;
	private NotificationService service = null;
	private Intent serviceIntent = null;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.testdrive);
		t = (TextView) findViewById(R.id.text);
		serviceIntent = new Intent(this, NotificationService.class);
		startService(serviceIntent);
		bindService(serviceIntent, connection, 0);
	}
	
	public void onDestroy() {
		unbindService(connection);
		super.onDestroy();
	}
	
	public void notifyUpdate(View v) {
		if(service != null) {
			service.showNotification();
		}
	}
	
	public void notifyHide(View v) {
		if(service != null) {
			service.hideNotification();
		}
	}
	
	public void showClassGuide(View v) {
		if(service != null) {
			service.showClassGuide("ClassGuide shows you the way!");
		}
	}
	
	public void hideClassGuide(View v) {
		if(service != null) {
			service.hideClassGuide();
		}
	}
	
	public void doakesUpdate(View v) {
		final NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		PendingIntent p = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), Main.class).putExtra("tobiass.rooster.NOTIFICATION_CLICK", true), 0);
		Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.somefries);
		Notification n = new NotificationCompat.Builder(getApplicationContext())
		.setTicker("Surprise Motherfucker!")
		.setContentIntent(p)
		.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(icon).setBigContentTitle("Surprise Motherfucker!"))
		.addAction(R.drawable.ic_action_discard, "Nope", PendingIntent.getActivity(getApplicationContext(), 0, new Intent(this, TestDrive.class), 0))
		.setSmallIcon(R.drawable.ic_action_select_all)
		.build();
		// unique id
		nm.notify(R.string._continue, n);
	}
	
	public void doakesHide(View v) {
		final NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(R.string._continue);
	}
	
	public void addFriends(View v) {
		new Thread(new Runnable() {
			public void run() {
				final Friends f = new Friends(TestDrive.this);
				School s = School.getSchool(TestDrive.this, 606);
				Iterator<String> i = s.getTeachers().iterator();
				
				while(i.hasNext()) {
					Friend a = new Friend();
					a.abbreviation = i.next();
					a.school = 606;
					a.type = Const.TYPE_TEACHER;
					f.addFriend(a);
				}
				f.close();
				
				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
					}
				});
			}
		}).start();
	}
	
	private void log(String text) {
		if(t != null) {
			t.setText(text);
		}
	}
	
	private ServiceConnection connection = new ServiceConnection() {
		public void onServiceDisconnected(ComponentName arg0) {
			log("Service disconnected");
			service = null;
		}
		
		public void onServiceConnected(ComponentName arg0, IBinder arg1) {
			log("Service connected");
			service = ((ServiceBinder)arg1).getService();
		}
	};
}
