package tobiass.rooster.splash;

import tobiass.rooster.R;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class SomeFries extends SherlockActivity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_somefries);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	public static void start(Context c) {
		c.startActivity(new Intent(c, SomeFries.class));
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return true;
	}
}
