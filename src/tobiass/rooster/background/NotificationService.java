package tobiass.rooster.background;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

import tobiass.rooster.R;
import tobiass.rooster.Viewer;
import tobiass.rooster.api.Day;
import tobiass.rooster.api.Friend;
import tobiass.rooster.api.Hour;
import tobiass.rooster.api.Lesson;
import tobiass.rooster.api.Request;
import tobiass.rooster.api.Request.onFinishedListener;
import tobiass.rooster.api.Schedule;
import tobiass.rooster.api.Week;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.Static;
import tobiass.rooster.storage.Friends;
import tobiass.rooster.storage.ScheduleCache;
import tobiass.rooster.storage.Timetables;
import tobiass.rooster.timetable.Timetable;
import tobiass.rooster.timetable.TimetableLesson;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class NotificationService extends Service {
	protected static final String TAG = Const.TAG+" service";
	public class ServiceBinder extends Binder {
		public NotificationService getService() {
            return NotificationService.this;
        }
    }
	private final IBinder mBinder = new ServiceBinder();
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	
	private Timer notificationTimer;
	private Timer classguideTimer;
	private boolean classguideOn;
	
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "Started");
		
		try {
			digest = MessageDigest.getInstance(Const.HASHING_ALGORITHM);
			if(emptyHash == null)
				emptyHash = new byte[digest.getDigestLength()];
		} catch (NoSuchAlgorithmException e1) {}
		
		updateSettings();
	}
	
	// Reads settings from preferences and start required timers etc
	public void updateSettings() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		classguideOn = prefs.getBoolean(Const.PREF_CLASSGUIDE, false);
		boolean notifications = classguideOn || prefs.getBoolean(Const.PREF_NOTIFICATIONS, false);
		
		// Stop
		if(!notifications && notificationTimer != null) {
			notificationTimer.cancel();
			notificationTimer = null;
			schedule = null;
			hash = null;
			hideNotification();
		}
		if(!classguideOn && classguideTimer != null) {
			classguideTimer.cancel();
			classguideTimer = null;
			isStartScheduled = false;
			hideClassGuide();
		}
		
		// Kill if nothing active
		if(!classguideOn && !notifications) {
			Log.d(TAG, "Stopped");
			stopSelf();
		}
		else {
			if(notifications && notificationTimer == null) {
				notificationTimer = new Timer();
				notificationTimer.scheduleAtFixedRate(new TimerTask() {
					public void run() {
						if(!busy) {
							getUpdate();
						}
					}
				}, 0, Const.DEFAULT_INTERVAL);
			}
			if(classguideOn && classguideTimer == null) { //notifications were already on, now classguide on
				scheduleClassGuide();
			}
		}
	}
	
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}
	
	public void hideNotification() {
		hideNotification(R.string.notifications);
	}
	
	public void hideClassGuide() {
		shownText = null;
		shownExpandable = null;
		hideNotification(R.string.classguide);
	}
	
	private void hideNotification(int id) {
		((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(id);
	}
	
	private Notification updateNotification;
	private Notification classguideNotification;
	
	public void showNotification() {
		final NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		PendingIntent p = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), Viewer.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra(Const.NOTIFICATION_CLICK, true), 0);
		updateNotification = new NotificationCompat.Builder(getApplicationContext())
		.setAutoCancel(true)
		.setTicker(getText(R.string.schedule_changed))
		.setContentText(getText(R.string.schedule_changed_msg))
		.setContentTitle(getText(R.string.schedule_changed))
		.setContentIntent(p)
		.setSmallIcon(R.drawable.ic_noti)
		.build();
		
		nm.notify(R.string.notifications, updateNotification);
	}
	
	private CharSequence shownText;
	private CharSequence shownExpandable;
	
	public void showClassGuide(CharSequence text, CharSequence expandable) {
		// We don't need the notification to be bumped to top every minute.
		if(shownText != null && shownExpandable != null && shownText.equals(text) && shownExpandable.equals(expandable))
			return;
		
		final NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		PendingIntent p = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), Viewer.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra(Const.NOTIFICATION_CLICK, true), 0);
		classguideNotification = new NotificationCompat.Builder(getApplicationContext())
		.setOngoing(true)
		.setTicker(getText(R.string.classguide_started))
		.setContentText(text)
		.setContentTitle(getText(R.string.classguide))
		.setContentIntent(p)
		.setStyle(expandable != null && expandable.length() > 0 ? new NotificationCompat.BigTextStyle().bigText(text+"\n"+expandable) : null)
		.setSmallIcon(R.drawable.ic_noti)
		.build();
		
		shownText = text;
		shownExpandable = expandable;

		nm.notify(R.string.classguide, classguideNotification);
	}
	
	public void showClassGuide(CharSequence text) {
		showClassGuide(text, text);
	}
	
	private void loadPreset() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String pref = prefs.getString(Const.PREF_TIMETABLE, null);
		if(pref == null)
			return;
		
		if(!pref.equals(loadedPreset)) {
			Timetables t = new Timetables(getApplicationContext());
			loadedPreset = pref;
			preset = t.getTimetable(pref);
			t.close();
		}
	}
	
	private String loadedPreset;
	private Timetable preset;
	private long lastRun;
	
	public synchronized void runClassGuide() {
		if(System.currentTimeMillis() < lastRun+60000)
			return;
		lastRun = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		
		int day = cal.get(Calendar.DAY_OF_WEEK);
		if(day < 2 || day > 6) {
			scheduleClassGuide(); // cancels current task as well, because it's weekend
			return;
		}
		
		if(facts == null)
			facts = getResources().getTextArray(R.array.classguide_enjoy_break);
		
		if(mRandom == null)
			mRandom = new Random();
		
		if(schedule == null) {
			Friends f = new Friends(getApplicationContext());
			Friend me = f.getMe();
			f.close();
			ScheduleCache cache = new ScheduleCache(getApplicationContext());
			try {
				schedule = cache.getSchedule(me.id, true, false);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			cache.close();
			
			if(schedule == null) {
				// allow request callback to run CG again.
				lastRun = 0;
				return;
			}
		}
		
		Log.d(TAG, "ClassGuide running");
		
		loadPreset();
		if(preset == null)
			return;
		
		int currentStamp = cal.get(Calendar.HOUR_OF_DAY)*60 + cal.get(Calendar.MINUTE);
		
		Iterator<TimetableLesson> iterator = preset.iterator();
		boolean upcoming = false;
		boolean withinRange = false;
		
		Day today = schedule.schedule.get(day - 2);
		byte firstLesson = today.getFirstLessonIndex();
		byte lastLesson = today.getLastLessonIndex();
		byte pointer = 0;
		Hour currentHour = null;
		TimetableLesson currentRange = null;
		
		// Select the current TimetableLesson and check whether it indicates the
		// upcoming lesson.
		while(iterator.hasNext()) {
			TimetableLesson range = iterator.next();
			if(currentStamp >= range.startStamp - 10 && currentStamp < range.startStamp + range.length - 10) {
				withinRange = true;
				byte lesson = preset.getBreakCorrectedPosition(pointer);
				if(lesson >= firstLesson && lesson <= lastLesson) {
					upcoming = currentStamp >= range.startStamp - 10 && currentStamp < range.startStamp;
					currentHour = today.get((int) lesson);
					currentRange = range;
				}
			}
			pointer++;
		}
		
		if(currentRange != null) {
			if(currentRange.isBreak) {
				if(fact == null) {
					fact = facts[mRandom.nextInt(facts.length-1)];
					showClassGuide(getString(R.string.classguide_break, currentRange.getEndTime()), fact);
				}
				return;
			}
			else if(currentHour != null) {
				fact = null; // make sure the fact will be refreshed next break.
				
				String time;
				if(upcoming)
					time = getString(R.string.classguide_start_at, currentRange.getStartTime());
				else
					time = getString(R.string.classguide_ends_at, currentRange.getEndTime());
				
				if(currentHour.isAllFree()) {
					showClassGuide(getText(R.string.classguide_free), time);
					return;
				}
				else {
					Lesson lesson = currentHour.get(0);
					if(lesson != null) {
						showClassGuide(getString(upcoming ? R.string.classguide_next_class : R.string.classguide_current_class, lesson.subject, lesson.classroom), time);
						return;
					}
				}
				
			}
		}
		else if(!withinRange)
			scheduleClassGuide();
		
		hideClassGuide();
	}
	
	public void rescheduleClassGuide() {
		Log.d(TAG, "Rescheduling ClassGuide start");
		isStartScheduled = false;
		lastRun = 0;
		scheduleClassGuide();
	}
	
	private boolean isStartScheduled;
	
	private void scheduleClassGuide() {
		if(isStartScheduled)
			return;
		
		if(classguideTimer != null)
			classguideTimer.cancel();
		classguideTimer = null;
		
		loadPreset();
		if(preset == null)
			return;
		
		classguideTimer = new Timer();
		
		Calendar cal = Calendar.getInstance();
		
		int range_start = preset.get(0).startStamp-10;
		TimetableLesson last = preset.get(preset.size()-1);
		int range_end = last.startStamp + last.length;
		
		int day = cal.get(Calendar.DAY_OF_WEEK);
		
		int currentStamp = cal.get(Calendar.HOUR_OF_DAY)*60 + cal.get(Calendar.MINUTE);
		
		if(day > 1 && day < 7 && currentStamp >= range_start && range_end > currentStamp) {
			Log.d(TAG, "Scheduled ClassGuide now");
			isStartScheduled = false;
			classguideTimer.scheduleAtFixedRate(new TimerTask() {
				public void run() {
					runClassGuide();
				}
			}, 0, 60000);
			return;
		}
		
		hideClassGuide();
		
		int delay = range_start - currentStamp;
		
		/**
		 * Hypothetically, when the time is 8:00, and the first lesson starts at
		 * 8:20, the delay is 10 minutes.
		 * 
		 * However, if the delay is negative, this means that the lessons for
		 * today have already passed. Therefore we need to add 1440 minutes to
		 * the delay (the number of minutes in a day). If the day is Saturday or
		 * Sunday after adding the 1440 minutes, we should add another 1440,
		 * until it's no longer weekend.
		 */
		
		if(delay < 0) {
			day++;
			delay += 1440;
			if(day > 7)
				day = 1;
			while(day < 2 || day > 6) {
				day++;
				delay += 1440;
				if(day > 7)
					day = 1;
			}
		}
		
		Date d = new Date(System.currentTimeMillis() + (60000 * delay));
		
		isStartScheduled = true;
		classguideTimer.schedule(new TimerTask() {
			public void run() {
				isStartScheduled = false;
				scheduleClassGuide();
			}
		}, d);
		
		Log.d(TAG, "Scheduled ClassGuide start at "+DateFormat.getInstance().format(d));
	}
	
	private CharSequence fact;
	private Random mRandom;
	private CharSequence[] facts;
	private int friend = -1;
	private int dayNo = -1;
	private byte[] hash;
	private Schedule schedule;
	private boolean busy;
	private static byte[] emptyHash;
	private MessageDigest digest;
	
	public void getUpdate() {		
		Friends f = new Friends(getApplicationContext());
		Friend me = f.getMe();
		f.close();
		
		if(me == null)
			return;
		
		final Request req = new Request();
		
		int currentDay = Static.getDayOfWeek();
		if(dayNo == -1) {
			dayNo = currentDay;
		}
		else if(currentDay != dayNo) {
			hash = emptyHash; // make sure user gets notified when there are changes on the next day
			dayNo = currentDay;
		}
		
		if(friend == -1) {
			friend = me.id;
		}
		else if(friend != me.id) {
			hash = null;
		}
		req.setFriend(me);
		
		req.setOnFinishedListener(new onFinishedListener() {
			public void onFinished() {
				if(req.error != null) {
					busy = false;
					return;
				}
				
				ScheduleCache s = new ScheduleCache(getApplicationContext());
				try {
					s.insert(req.result);
					schedule = req.result;
					if(classguideOn)
						runClassGuide();
				}
				catch (IOException e) {}
				catch (JSONException e) {}
				
				s.close();
				byte[] latestHash = getHash(req.result.schedule);
				if(hash == null) {
					hash = latestHash;
				}
				else if(!Arrays.equals(latestHash, hash)) {
					hash = latestHash;
					showNotification();
				}
				busy = false;
			}
		});
		busy = true;
		req.run(this);
	}
	
	private byte[] getHash(Week schedule) {
		int digestLength = digest.getDigestLength();
		byte[] hash = new byte[digestLength];
		
		Day day = schedule.get(dayNo);
		
		Iterator<Hour> hours = day.iterator();
		
		int index = 0;
		while(hours.hasNext()) {
			Hour hour = hours.next();
			Iterator<Lesson> lessons = hour.iterator();
			while(lessons.hasNext()) {
				Lesson lesson = lessons.next();
				if(lesson.updated) {
					// Merge hashes
					byte[] arr = new byte[digestLength * 2];
					System.arraycopy(hash, 0, arr, 0, digestLength);
					byte[] unique = lesson.hash(index);
					System.arraycopy(unique, 0, arr, digestLength, digestLength);
					hash = digest.digest(arr);
				}
				index++;
			}
		}
		Log.d(TAG+" hasher", "Created hash "+Static.md5toString(hash));
		return hash;
	}
}
