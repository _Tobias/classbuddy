package tobiass.rooster.api;

import java.util.ArrayList;
import java.util.Collection;

public class Week extends ArrayList<Day> {
	private static final long serialVersionUID = 6365568448354369479L;
	
	public Week() {
		super();
	}
	
	public Week(int capacity) {
		super(capacity);
	}
	
	public Week(Collection<? extends Day> collection) {
		super(collection);
	}
}
