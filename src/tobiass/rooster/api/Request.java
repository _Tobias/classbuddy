package tobiass.rooster.api;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tobiass.rooster.R;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.Static;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

public class Request {
	public int type;
	public String department;
	public String abbreviation;
	public int student_id = 0;
	public String _class;
	public String classroom;
	
	public boolean changes = true;
	public boolean async = true;
	public int school;
	public Schedule result;
	public Friend friend;
	public String error;
	
	public void setFriend(Friend f) {
		friend = f;
		type = f.type;
		school = f.school;
		switch(f.type) {
		case Const.TYPE_CLASS:
			_class = f._class;
			break;
		case Const.TYPE_STUDENT:
			department = f.department;
			student_id = f.student_id;
			break;
		case Const.TYPE_TEACHER:
			abbreviation = f.abbreviation;
			break;
		case Const.TYPE_CLASSROOM:
			classroom = f.classroom;
			break;			
		}
	}
	
	public void run(final Context context) {
		result = null;
		error = null;
		Runnable r = new Runnable() {
			public void run() {
				final Friend f = new Friend();
				f.id = -1;
				f.school = school;
				f.type = type;
				String ts = null;
				String data = null;
				switch(type) {
				case Const.TYPE_TEACHER:
					ts = "Docent";
					data = "docenten="+abbreviation;
					f.abbreviation = abbreviation;
					f.type = Const.TYPE_TEACHER;
					break;
				case Const.TYPE_CLASS:
					ts = "Klas";
					data = "klassen="+_class;
					f._class = _class;
					f.type = Const.TYPE_CLASS;
					break;
				case Const.TYPE_STUDENT:
					ts = "Leerling";
					try {
						data = "afdeling="+URLEncoder.encode(department, "UTF-8")+"&leerling="+student_id;
					} catch (UnsupportedEncodingException e1) {	}
					f.department = department;
					f.student_id = student_id;
					f.type = Const.TYPE_STUDENT;
					break;
				case Const.TYPE_CLASSROOM:
					ts = "Lokaal";
					data = "lokalen="+classroom;
					f.classroom = classroom;
					break;
				}
				ts += "rooster";
				try {
					final String url = "http://gepro.nl/roosters/rooster.php?school="+school+
							"&wijzigingen="+(changes ? "1" : "0")+
							"&type="+ts+
							"&"+data;
					Log.d(Const.TAG, "Request: "+url);
					final HttpURLConnection conn = ((HttpURLConnection) new URL(url).openConnection());
					final String html = Static.convertStream(conn.getInputStream());
					
					result = new Schedule();
					result.lastMod = Static.getBetween("Laatst bijgewerkt: ", "&nbsp;", html);
					result.lastMod = result.lastMod.substring(0, result.lastMod.length()-3);
					
					// populate friend with name data
					if(type == Const.TYPE_STUDENT) {
						String[] s = (String[]) Friend.nameConverter.convert(html);
						if(s != null) {
							f.name = s[0];
							f._class = s[1];
						}
						else {
							error = context.getString(R.string.error_student_non_existent);
						}
					}
					
					if(friend != null && friend.name != null) {
						f.name = friend.name;
						f.id = friend.id;
					}
					result.changes = changes;
					result.friend = f;
					result.schedule = parseRooster(html);
				}
				catch (Exception e) {
					if(context == null) {
						error = e.getClass().getName()+"\nGeen Context in Request";
					}
					else {
						error = Static.handleException(context, e);
					}
				}
				
				if(async && context != null && context instanceof Activity) {
					((Activity) context).runOnUiThread(onFinishedRunnable);
				}
				else {
					onFinishedRunnable.run();
				}
			}
		};
		
		if(async) {
			new Thread(r).start();
		}
		else {
			r.run();
		}
	}
	
	private Runnable onFinishedRunnable = new Runnable() {
		public void run() {
			if(onFinishedListener != null) {
				onFinishedListener.onFinished();
			}
		}
	};
	
	private static Pattern pattern1 = Pattern.compile("<td align=\"left\" width=\"auto\" class=\"tableCell\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >(.+?)</table>", Pattern.DOTALL);
	private static Pattern patternTr = Pattern.compile("<tr>(.+?)</tr>", Pattern.DOTALL);
	private static Pattern patternTd = Pattern.compile("<td.+?>(.+?)</td>");
	private static Pattern patternError = Pattern.compile(">([^<]+?)$");
	
	// Haalt een rooster uit de html
	@SuppressLint("DefaultLocale")
	private Week parseRooster(String html) {
		int dag = 0;
		
		Week week = new Week(5);
		for(int i = 0; i < 5; i++) {
			week.add(new Day());
		}
		
		final Matcher m = pattern1.matcher(html);
		
		// Loop through entries
		boolean got = false;
		while(m.find()) {
			got = true;
			String entry = m.group(1);
			Matcher lesMatcher = patternTr.matcher(entry);
			Hour entryList = new Hour();
			
			// Loop through lessen in entry
			while(lesMatcher.find()) {
				Lesson les = new Lesson();
				String tr = lesMatcher.group(1);
				les.updated = tr.contains("tableCellNew") || tr.contains("tableCellRemoved");
				Matcher tdMatcher = patternTd.matcher(tr);
				// Loop through TDs in les
				int infc = 0;
				while(tdMatcher.find()) {
					
					// Parse all the TDs!
					String tdc = tdMatcher.group(1);
					if(!tdc.equals("&nbsp") && !tdc.equals(".")) {
						if(tdc.equals("vrij") || tdc.toLowerCase(Locale.US).indexOf("vrij") == 0) {
							les.free = true;
						}
						else {
							switch(infc) {
							case 0:
								if(type == Const.TYPE_TEACHER) {
									les._class = tdc;
								}
								else {
									les.teacher = tdc;
								}
								break;
							case 1:
								if(type == Const.TYPE_CLASSROOM) {
									les._class = tdc;
								}
								else {
									les.classroom = tdc;
								}
								break;
							case 2: les.subject = tdc; break;
							case 3: les.cluster = tdc; break;
							}
							infc++;
						}
					}
				}
				
				if(infc == 0) {
					les.free = true;
				}
				
				entryList.add(les);
			}
			week.get(dag).add(entryList);
			dag++;
			if(dag == 5) {
				dag = 0;
			}
		}
		if(!got) {
			Matcher e = patternError.matcher(html);
			e.find();
			if(e.group(1) != null) {
				error = e.group(1).trim().replace("  ", " ");
			}
			return null;
		}
		else {
			// dagen is populated hier
			
			for(int a = 0; a < 5; a++) {
				Day d = week.get(a);
				for(int b = d.size()-1; b >= 0; b--) {
					ArrayList<Lesson> u = d.get(b);
					boolean alleVrij = true;
					for(Lesson l : u) {
						if(!(l.free && !l.updated)) {
							alleVrij = false;
						}
					}
					if(alleVrij) {
						d.remove(b);
					}
					else {
						break;
					}
				}
			}
		}
		return week;
	}
	
	// Callback voor wanneer een request klaar is.
	
	private onFinishedListener onFinishedListener = null;
	public void setOnFinishedListener(onFinishedListener arg) {
		onFinishedListener = arg;
	}
	
	public interface onFinishedListener {
		public void onFinished();
	}
}
