package tobiass.rooster.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tobiass.rooster.R;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.Static;
import tobiass.rooster.storage.WebStore;
import tobiass.rooster.storage.WebStore.Cacheable;
import tobiass.rooster.storage.WebStore.Callback;
import tobiass.rooster.storage.WebStore.Converter;
import android.app.Activity;
import android.content.Context;
import android.util.SparseArray;

public class School {
	
	private static final String DEPARTMENTS = "a";
	private static final String CLASSES = "b";
	private static final String TEACHERS = "c";
	private static final String CLASSROOMS = "d";
	private static final String ID = "e";
	
	public int error = -1;
	private List<String> classes;
	private List<String> departments;
	private List<String> teachers;
	private List<String> classrooms;
	private static Pattern p = Pattern.compile("<option>(.+?)</option>");
	private WebStore resources;
	public int id;
	private static int cache = 1800;
	@SuppressWarnings("unchecked")
	public School(int id) {
		this.id = id;
		resources = WebStore.getInstance();
		resources.get("http://gepro.nl/roosters/rooster?school="+id, null, optionItemLister, new Callback() {
			public void callback(Cacheable data, Exception e, boolean mainThread) {
				if(e != null) {
					error = R.string.error_class_list;
				}
				else {
					classes = (List<String>) data.converted;
				}
			}
		}, cache, false);
		
		if(error > -1) {
			return;
		}
		
		resources.get("http://gepro.nl/roosters/rooster?type=Docentrooster&school="+id, null, optionItemLister, new Callback() {
			public void callback(Cacheable data, Exception e, boolean mainThread) {
				if(e != null) {
					error = R.string.error_teacher_list;
				}
				else {
					teachers = (List<String>) data.converted;
				}
			}
		}, cache, false);
		
		if(error > -1) {
			return;
		}
		
		resources.get("http://gepro.nl/roosters/rooster?type=Leerlingrooster&school="+id, null, optionItemLister, new Callback() {
			public void callback(Cacheable data, Exception e, boolean mainThread) {
				if(e != null) {
					error = R.string.error_department_list;
				}
				else {
					departments = (List<String>) data.converted;
				}
			}
		}, cache, false);
		
		if(error > -1) {
			return;
		}
		
		resources.get("http://gepro.nl/roosters/rooster?type=Lokaalrooster&school="+id, null, optionItemLister, new Callback() {
			public void callback(Cacheable data, Exception e, boolean mainThread) {
				if(e != null) {
					error = R.string.error_department_list;
				}
				else {
					classrooms = (List<String>) data.converted;
				}
			}
		}, cache, false);
		
		if(error > -1) {
			return;
		}
		
		if(departments.size() == 0 && classes.size() == 0 && teachers.size() == 0 && classrooms.size() == 0) {
			error = R.string.error_school_non_existent;
		}
	}
	
	public School(int id, String[] classes, String[] departments, String[] teachers, String[] classrooms) {
		this.id = id;
		this.classes = new ArrayList<String>();
		this.teachers = new ArrayList<String>();
		this.departments = new ArrayList<String>();
		this.classrooms = new ArrayList<String>();
		for(String _class : classes) {
			this.classes.add(_class);
		}
		for(String dept : departments) {
			this.departments.add(dept);
		}
		for(String teacher : teachers) {
			this.teachers.add(teacher);
		}
		for(String room : classrooms) {
			this.classrooms.add(room);
		}
	}
	
	public School(String jsonString) throws JSONException {
		JSONObject obj = new JSONObject(jsonString);
		id = obj.getInt(ID);
		departments = convertJSONArrayToList(obj.getJSONArray(DEPARTMENTS));
		teachers = convertJSONArrayToList(obj.getJSONArray(TEACHERS));
		classrooms = convertJSONArrayToList(obj.getJSONArray(CLASSROOMS));
		classes = convertJSONArrayToList(obj.getJSONArray(CLASSES));
	}
	
	private static List<String> convertJSONArrayToList(JSONArray arr) {
		List<String> list = new ArrayList<String>();
		try {
			for(int i = 0; i < arr.length(); i++) {
				list.add(arr.getString(i));
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}
	
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		try {
			obj.put(ID, id);
			obj.put(DEPARTMENTS, new JSONArray(departments));
			obj.put(TEACHERS, new JSONArray(teachers));
			obj.put(CLASSROOMS, new JSONArray(classrooms));
			obj.put(CLASSES, new JSONArray(classes));
		} catch (JSONException e) {
			return null;
		}
		return obj;
	}
	
	public List<String> getClasses() {
		return classes;
	}
	
	public List<String> getDepartments() {
		return departments;
	}
	
	public List<String> getTeachers() {
		return teachers;
	}
	
	public List<String> getClassrooms() {
		return classrooms;
	}
	
	private static final Converter optionItemLister = new Converter() {
		public Object convert(String input) {
			List<String> classes = new ArrayList<String>();
			Matcher m = p.matcher(input);
			while(m.find()) {
				classes.add(m.group(1));
			}
			return classes;
		}
	};
	private static SparseArray<School> cachedSchools = new SparseArray<School>();
	public static void clearMemCache() {
		cachedSchools.clear();
		cachedSchoolList = null;
	}
	
	public static void clearAllCache(Context context) {
		clearMemCache();
		File dir = new File(context.getFilesDir()+"/");
		for(File f : dir.listFiles()) {
			f.delete();
		}
	}
	public static void clearJsonCache(Context context, int id) {
		new File(context.getFilesDir()+"/school_"+id).delete();
	}
	
	public static boolean hasCached(int id) {
		return cachedSchools.indexOfKey(id) > -1;
	}
	
	public static School getSchool(Activity activity, int id) {
		if(hasCached(id)) {
			return cachedSchools.get(id);
		}
		File school = activity.getFileStreamPath("school_"+id);
		if(school.exists()) {
			try {
				FileInputStream fis = activity.openFileInput(school.getName());
				School s = new School(Static.convertStream(fis));
				cachedSchools.put(id, s);
				return s;
				
			}
			catch (FileNotFoundException e) {}
			catch (JSONException e) {}
		}
		else {
			School s = new School(id);
			if(s.error == -1) {
				try {
					FileOutputStream fos = activity.openFileOutput("school_"+id, 0);
					fos.write(s.toJSON().toString().getBytes());
					fos.close();
				}
				catch (IOException e) {}
				cachedSchools.put(id, s);
			}
			return s;
		}
		return null;
	}
	
	private static SparseArray<String> cachedSchoolList = null;
	public static boolean hasSchoolListCached() {
		return cachedSchoolList != null;
	}
	
	private static final String schoolsUrl = "http://tobiass.nl/rapi/s";
	private static final String schoolsFile = "school_list";
	@SuppressWarnings("unchecked")
	public static SparseArray<String> getList(final Activity activity) {
		if(cachedSchoolList != null) {
			return cachedSchoolList;
		}
		File f = activity.getFileStreamPath(schoolsFile);
		if(f.exists() && (System.currentTimeMillis()-f.lastModified()) < Const.EXPIRY_SCHOOL_LIST) {
			try {
				cachedSchoolList = (SparseArray<String>) schoolsFileConverter.convert(Static.convertStream(activity.openFileInput(schoolsFile)));
				return cachedSchoolList;
			} catch (FileNotFoundException e1) {}
		}
		final WebStore resources = WebStore.getInstance();
		resources.get(schoolsUrl, null, schoolsFileConverter, new Callback() {
			public void callback(Cacheable data, Exception e, boolean mainThread) {
				if(e == null) {
					cachedSchoolList = (SparseArray<String>) data.converted;
					try {
						FileOutputStream fos = activity.openFileOutput(schoolsFile, 0);
						fos.write(data.data.getBytes());
						fos.close();
					} catch (IOException e1) {}
				}
			}
		}, 0, false, "UTF-8");
		
		return cachedSchoolList;
	}
	
	private static Converter schoolsFileConverter = new Converter() {
		public Object convert(String input) {
			SparseArray<String> s = new SparseArray<String>();
			for(String line : input.split("\n")) {
				String[] split = line.split("\\|");
				s.put(Integer.valueOf(split[1]), split[0]);
			}
			return s;
		}
	};
}