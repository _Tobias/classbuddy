package tobiass.rooster.api;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONWriter {
	private JSONObject mObject;
	public JSONWriter(JSONObject object) {
		mObject = object;
	}
	
	public JSONWriter put(String name, boolean value) throws JSONException {
		if(value) {
			mObject.put(name, value);
		}
		return this;
	}
	
	public JSONWriter put(String name, String value) throws JSONException {
		if(value != null) {
			mObject.put(name, value);
		}
		return this;
	}
	
	public JSONWriter put(String name, int value) throws JSONException {
		if(value != 0) {
			mObject.put(name, value);
		}
		return this;
	}
	
	public JSONWriter put(String name, byte value) throws JSONException {
		if(value != 0) {
			mObject.put(name, (int) value);
		}
		return this;
	}
	
	public JSONWriter put(String name, Object value) throws JSONException {
		mObject.put(name, value);
		return this;
	}
	
	public JSONObject getJSONObject() {
		return mObject;
	}
}
