package tobiass.rooster.api;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Class that represents a day in a {@link Week}. Extends {@link ArrayList} of type {@link Hour}
 */
@SuppressWarnings("serial")
public class Day extends ArrayList<Hour> {
	public JSONArray serialize() throws JSONException {
		JSONArray out = new JSONArray();
		Iterator<Hour> arrayListIterator = iterator();
		
		while(arrayListIterator.hasNext()) {
			ArrayList<Lesson> arrayList = arrayListIterator.next();
			JSONArray out2 = new JSONArray();
			Iterator<Lesson> lessonIterator = arrayList.iterator();
			while(lessonIterator.hasNext()) {
				Lesson lesson = lessonIterator.next();
				out2.put(lesson.serialize());
			}
			out.put(out2);
		}
		return out;
	}
	
	public Day() {
		super();
	}
	
	public Day(JSONArray in) throws JSONException {
		super();
		for(int i = 0; i < in.length(); i++) {
			JSONArray hour = in.getJSONArray(i);
			Hour array = new Hour(hour.length());
			for(int y = 0; y < hour.length(); y++) {
				array.add(new Lesson(hour.getJSONObject(y)));
			}
			add(array);
		}
	}
	
	public byte getFirstLessonIndex() {
		Iterator<Hour> it = iterator();
		byte pos = 0;
		while(it.hasNext()) {
			Hour hour = it.next();
			if(!hour.isAllFree())
				break;
			pos++;
		}
		return pos;
	}
	
	public byte getLastLessonIndex() {
		Iterator<Hour> it = iterator();
		byte pos = 0;
		byte last = 0;
		while(it.hasNext()) {
			if(!it.next().isAllFree())
				last = pos;
			pos++;
		}
		return last;
	}
}
