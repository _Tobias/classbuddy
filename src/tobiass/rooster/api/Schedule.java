package tobiass.rooster.api;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;

import tobiass.rooster.misc.Const;
import tobiass.rooster.storage.DB;
import android.content.ContentValues;

public class Schedule {
	public String lastMod;
	public Friend friend;
	public boolean changes;
	
	public Week schedule = null;
	
	public ContentValues getContentValues() {
		// 5
		ContentValues cv = new ContentValues(5);
		cv.put(DB.CFRIEND_ID, friend.id);
		cv.put(DB.CCHANGES, changes);
		cv.put(DB.CEXPIRY, Math.floor(System.currentTimeMillis()/1000)+(Const.EXPIRY_CACHE));
		cv.put(DB.CLASTMOD, lastMod);
		return cv;
	}
	
	public Schedule() {}
	
	public Schedule(String json) throws JSONException {
		schedule = new Week(5);
		JSONArray in = new JSONArray(json);
		for(int i = 0; i < in.length(); i++) {
			JSONArray day = in.getJSONArray(i);
			schedule.add(new Day(day));
		}
	}
	
	public JSONArray serialize() throws JSONException {
		JSONArray out = new JSONArray();
		
		Iterator<Day> dayIterator = schedule.iterator();
		while(dayIterator.hasNext()) {
			Day day = dayIterator.next();
			out.put(day.serialize());
		}
		
		return out;
	}
}