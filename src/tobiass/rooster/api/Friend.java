package tobiass.rooster.api;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import tobiass.rooster.R;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.Static;
import tobiass.rooster.storage.DB;
import tobiass.rooster.storage.WebStore;
import tobiass.rooster.storage.WebStore.Cacheable;
import tobiass.rooster.storage.WebStore.Callback;
import tobiass.rooster.storage.WebStore.Converter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 * Class containing a friend. {@link Friend}.equals can be used to compare this object to
 * others. Object can be serialized and deserialized from and to a
 * {@link JSONObject}.
 */

public class Friend {	
	public int id = 0;
	public int type = 0;
	public int student_id = 0;
	public int school = 0;
	public String abbreviation = null;
	public String name = null;
	public String department = null;
	public String _class = null;
	
	public String classroom = null;
	
	
	public boolean equals(Object object) {
		Friend o = (Friend) object;
		
		if(o.type == type && o.school == school) {
			switch(type) {
			case Const.TYPE_CLASS:
				return o._class.equals(_class);
			case Const.TYPE_CLASSROOM:
				return o.classroom.equals(classroom);
			case Const.TYPE_STUDENT:
				return o.student_id == student_id && o.department.equals(department);
			case Const.TYPE_TEACHER:
				return o.abbreviation.equals(abbreviation);
			}
		}
			
		return false;
	}
	
	public Friend() {}
	
	public Friend(JSONObject obj) {
		id = obj.optInt(Const.ID);
		type = obj.optInt(Const.TYPE);
		student_id = obj.optInt(Const.STUDENT_ID);
		school = obj.optInt(Const.SCHOOL);
		abbreviation = obj.optString(Const.ABBREVIATION, null);
		name = obj.optString(Const.NAME, null);
		department = obj.optString(Const.DEPARTMENT, null);
		_class = obj.optString(Const.CLASS, null);
		classroom = obj.optString(Const.CLASSROOM, null);
	}
	
	public JSONObject json() throws JSONException {
		return new JSONWriter(new JSONObject())
		.put(Const.ID, id)
		.put(Const.TYPE, type)
		.put(Const.STUDENT_ID, student_id)
		.put(Const.SCHOOL, school)
		.put(Const.ABBREVIATION, abbreviation)
		.put(Const.NAME, name)
		.put(Const.DEPARTMENT, department)
		.put(Const.CLASS, _class)
		.put(Const.CLASSROOM, classroom)
		.getJSONObject();
	}
	
	public String error = null;
	public String addName(final Activity activity) {
		error = null;
		WebStore res = WebStore.getInstance();
		try {
			res.get("http://gepro.nl/roosters/rooster.php?school="+school+"&type=Leerlingrooster&afdeling="+URLEncoder.encode(department, "UTF-8")+"&leerling="+student_id,
					null, nameConverter, new Callback() {
						public void callback(Cacheable data, Exception e, boolean mainThread) {
							if(e == null && data.converted != null) {
								String[] ret = (String[]) data.converted;
								
								Friend.this.name = ret[0];
								Friend.this._class = ret[1];
							}
							else {
								if(e != null) {
									error = Static.handleException(activity, e);
								}
								else {
									error = activity.getString(R.string.error_student_non_existent);
								}
							}
						}
					}, 0, false);
		} catch (UnsupportedEncodingException e) {}
		return error;
	}
	
	public String toString() {
		return name != null ? name : (_class != null ? _class : (abbreviation != null ? abbreviation : (classroom != null ? classroom : null)));
	}
	
	public final static Converter nameConverter = new Converter() {
		public Object convert(String input) {
			String[] ret = new String[2];
			ret[0] = null;
			ret[1] = null;
			
			String name = Static.getBetween("<td class=\"lNameHeader\" colspan=\"6\">", "</td>", input);
			if(name == null) {
				return null;
			}
			String[] s = name.split(" - ");
			for(String b : s) {
				if(b.matches("^[^0-9]+$")) {
					//name
					ret[0] = b;
				}
				else {
					// class
					ret[1] = b;
				}
			}
			return ret[0] != null && ret[1] != null ? ret : null;
		}
	};
	
	public void share(Context c) {
		ShortenShareUrl shortener = new ShortenShareUrl();
		shortener.setFriendAndContext(this, c);
		shortener.execute();
	}
	
	public static void share(Context c, Friend[] friends) {
		ShortenShareUrl shortener = new ShortenShareUrl();
		shortener.setFriendsAndContext(friends, c);
		shortener.execute();
	}
	
	private static class ShortenShareUrl extends AsyncTask<Void, Void, String> {
		private Context mContext;
		private ProgressDialog mLoadingDialog;
		private String mURL = "http://c.pi1.nl";
		private String mShortened;
		private Friend[] mFriends;
		
		public void setFriendsAndContext(Friend[] friends, Context c) {
			mFriends = friends;
			mContext = c;
		}
		
		public void setFriendAndContext(Friend friend, Context c) {
			mFriends = new Friend[1];
			mFriends[0] = friend;
			mContext = c;
		}
		
		protected void onPreExecute() {
			mLoadingDialog = ProgressDialog.show(mContext, mContext.getText(R.string.generating_link), mContext.getText(R.string.just_a_sec), true, false);
		}
		
		protected String doInBackground(Void... params) {
			try {
				for(Friend f : mFriends) {
					mURL += "/" + URLEncoder.encode(f.json().toString(), Const.UTF8_CHARSET);
				}
			}
			catch (UnsupportedEncodingException e) {
				return null;
			}
			catch (JSONException e) {
				return null;
			}
			
			WebStore store = WebStore.getInstance();
			store.get("http://is.gd/create.php?format=simple&url="+mURL, null, null, new Callback() {
				public void callback(Cacheable data, Exception e, boolean mainThread) {
					if(e != null) {
						mShortened = mURL;
					}
					else {
						mShortened = data.data;
					}
				}
			}, 0, false, Const.UTF8_CHARSET);
			return mShortened;
		}
		
		protected void onPostExecute(String result) {
			//trollfix
			try {
				mLoadingDialog.dismiss();
			}
			catch(IllegalArgumentException e) {}
			
			
			if(result == null) {
				Toast.makeText(mContext, R.string.error_generating_link, Toast.LENGTH_SHORT).show();
				return;
			}
			
			Static.shareText(mContext, (mFriends.length == 1 ? mFriends[0].toString() : mContext.getString(R.string.app_name))+"\n"+result, null);
		}
	}
	
	public ContentValues getContentValues() {
		ContentValues c = new ContentValues();
		c.put(DB.CTYPE, type);
		c.put(DB.CCLASS, _class);
		c.put(DB.CABBREVIATION, abbreviation);
		c.put(DB.CDEPARTMENT, department);
		c.put(DB.CNAME, name);
		c.put(DB.CSTUDENT_ID, student_id);
		c.put(DB.CSCHOOL, school);
		return c;
	}
}
