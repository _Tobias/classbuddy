package tobiass.rooster.api;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class contains one lesson. It can be serialized to a {@link JSONObject}
 * and can be hashed to a {@link byte[16]}. It can also be initialized with a
 * {@link JSONObject} as parameter. The {@link JSONObject}'s data will be saved
 * in the {@link Lesson}.
 */
public class Lesson {	
	private static final String FREE = "a";
	private static final String UPDATED = "b";
	private static final String _CLASS = "c";
	private static final String CLASSROOM = "d";
	private static final String TEACHER = "e";
	private static final String CLUSTER = "f";
	private static final String SUBJECT = "g";
	private static final String INDEX = "h";
	
	public boolean free = false;
	public boolean updated = false;
	public String _class = null;
	public String classroom = null;
	public String teacher = null;
	public String cluster = null;
	public String subject = null;
	
	private static String HASH_ALGO = "MD5";
	public byte[] hash(int index) {
		try {
			return MessageDigest.getInstance(HASH_ALGO).digest(serialize().put(INDEX, index).toString().getBytes());
		}
		catch (NoSuchAlgorithmException e) {}
		catch (JSONException e) {}
		return null;
	}
	
	public Lesson() {}
	
	public Lesson(JSONObject obj) throws JSONException {
		free = obj.optBoolean(FREE);
		updated = obj.optBoolean(UPDATED);
		_class = obj.optString(_CLASS);
		classroom = obj.optString(CLASSROOM);
		teacher = obj.optString(TEACHER);
		cluster = obj.optString(CLUSTER);
		subject = obj.optString(SUBJECT);
	}
	
	public JSONObject serialize() throws JSONException {
		JSONWriter writer = new JSONWriter(new JSONObject());
		writer.put(FREE, free)
		.put(UPDATED, updated)
		.put(_CLASS, _class)
		.put(CLASSROOM, classroom)
		.put(TEACHER, teacher)
		.put(CLUSTER, cluster)
		.put(SUBJECT, subject);
		return writer.getJSONObject();
	}
}