package tobiass.rooster.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Class that represents one hour slot in a {@link Day}. Contains {@link Lesson} objects.
 */

@SuppressWarnings("serial")
public class Hour extends ArrayList<Lesson> {
	public Hour() {
		super();
	}
	
	public Hour(int capacity) {
		super(capacity);
	}
	
	public Hour(Collection<? extends Lesson> collection) {
		super(collection);
	}
	
	public boolean isAllFree() {
		Iterator<Lesson> lessons = iterator();
		boolean allFree = true;
		while(lessons.hasNext()) {
			if(!lessons.next().free)
				allFree = false;
		}
		return allFree;
	}
}
