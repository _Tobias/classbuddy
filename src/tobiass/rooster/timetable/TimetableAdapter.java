package tobiass.rooster.timetable;

import tobiass.rooster.R;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TimetableAdapter extends ArrayAdapter<TimetableLesson> {
		private Timetable mTable;
		public TimetableAdapter(Context context, Timetable map) {
			super(context, 0);
			mTable = map == null ? new Timetable() : map;
		}
		
		public int getCount() {
			return mTable.size();
		}
		
		public void add(TimetableLesson object) {
			mTable.add(object);
			notifyDataSetChanged();
		}
		
		public void remove(TimetableLesson object) {
			mTable.remove(object);
			notifyDataSetChanged();
		}
		
		public Timetable getTimetable() {
			return mTable;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView v = (TextView) convertView;
			if(v == null) {
				v = (TextView) View.inflate(getContext(), android.R.layout.simple_list_item_1, null);
			}
			TimetableLesson lesson = mTable.get((byte) position);
			if(lesson.isBreak)
				v.setText(String.format(getContext().getString(R.string._break_times), lesson.getStartTime(), lesson.getEndTime()));
			else
				v.setText(String.format(getContext().getString(R.string.lesson_times), (int) mTable.getBreakCorrectedPosition((byte) position) + 1, lesson.getStartTime(), lesson.getEndTime()));
			return v;
		}
}
