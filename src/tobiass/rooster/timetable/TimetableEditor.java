package tobiass.rooster.timetable;

import tobiass.rooster.R;
import tobiass.rooster.background.NotificationService;
import tobiass.rooster.background.NotificationService.ServiceBinder;
import tobiass.rooster.misc.Const;
import tobiass.rooster.storage.Timetables;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;

public class TimetableEditor extends SherlockListActivity implements OnNavigationListener {
	private Timetable mTimetable;
	private String[] mPresetNames;
	private Timetables mDb;
	private SharedPreferences mPrefs;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		ActionBar bar = getSupportActionBar();
		bar.setTitle(null);
		bar.setDisplayHomeAsUpEnabled(true);
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		
		serviceIntent = new Intent(this, NotificationService.class);
	}
	
	private Intent serviceIntent;
	public NotificationService service;
	private ServiceConnection connection = new ServiceConnection() {
		public void onServiceDisconnected(ComponentName arg0) {
			service = null;
		}
		
		public void onServiceConnected(ComponentName arg0, IBinder arg1) {
			service = ((ServiceBinder)arg1).getService();
		}
	};
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			onBackPressed();
		}
		return true;
	}
	
	public void onResume() {
		super.onResume();
		mDb = new Timetables(this);
		
		bindService(serviceIntent, connection, 0);
		startService(serviceIntent);
		
		reloadPresets();
	}
	
	public void onPause() {
		mDb.close();
		unbindService(connection);
		super.onPause();
	}
	
	private void reloadPresets() {
		mPresetNames = mDb.getPresetNames();
		ArrayAdapter<String> adapter = new PresetAdapter(this, mPresetNames);
		adapter.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
		getSupportActionBar().setListNavigationCallbacks(adapter, this);
		
		String pref = mPrefs.getString(Const.PREF_TIMETABLE, null);
		if(pref != null) {
			int p = 0;
			for(String s : mPresetNames) {
				if(pref.equals(s)) {
					getSupportActionBar().setSelectedNavigationItem(p);
					break;
				}
				p++;
			}
		}
	}
	
	private MenuItem mAddButton;
	private MenuItem mDeleteButton;
	public boolean onCreateOptionsMenu(Menu menu) {
		mAddButton = menu.add(R.string.timetable_add_preset);
		mAddButton.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		mAddButton.setIcon(R.drawable.ic_action_new);
		mAddButton.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				startActivity(new Intent(TimetableEditor.this, TimetableCreator.class));
				return true;
			}
		});
		mDeleteButton = menu.add(R.string.timetable_delete_preset);
		mDeleteButton.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		mDeleteButton.setIcon(R.drawable.ic_action_discard);
		mDeleteButton.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				new AlertDialog.Builder(TimetableEditor.this)
				.setMessage(R.string.timetable_delete_warning)
				.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						mDb.delete(mPresetNames[getSupportActionBar().getSelectedNavigationIndex()]);
						reloadPresets();
					}
				})
				.setNegativeButton(R.string.cancel, null)
				.show();
				return true;
			}
		});
		return true;
	}
	
	public boolean onPrepareOptionsMenu(Menu menu) {
		@SuppressWarnings("static-access")
		boolean sys = mDb.PRESETS.length > getSupportActionBar().getSelectedNavigationIndex();
		mDeleteButton.setVisible(!sys);
		mDeleteButton.setEnabled(!sys);
		return true;
	}
	
	private class PresetAdapter extends ArrayAdapter<String> {
		public PresetAdapter(Context context, String[] names) {
			super(context, R.layout.sherlock_spinner_item, names);
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView v = (TextView) super.getView(position, convertView, parent);
			v.setTextColor(Color.WHITE);
			return v;
		}
		
		public View getDropDownView(int position, View convertView, ViewGroup parent) {
			TextView v = (TextView) super.getDropDownView(position, convertView, parent);
			v.setTextColor(Color.WHITE);
			return v;
		}
	}

	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		supportInvalidateOptionsMenu();
		boolean first = mTimetable == null;
		mTimetable = mDb.getTimetable(mPresetNames[itemPosition]);
		setListAdapter(new TimetableAdapter(this, mTimetable));
		mPrefs.edit().putString(Const.PREF_TIMETABLE, mPresetNames[itemPosition]).commit();
		
		if(service != null && !first)
			service.rescheduleClassGuide();
		return true;
	}
}
