package tobiass.rooster.timetable;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;

@SuppressWarnings("serial")
public class Timetable extends ArrayList<TimetableLesson> {
	public void add(boolean isBreak, byte startHour, byte startMinute, int length) {
		add(new TimetableLesson(isBreak, startHour, startMinute, length));
	}
	
	public byte getBreakCorrectedPosition(byte position) {
		byte breaks = 0;
		byte pointer = 0;
		Iterator<TimetableLesson> values = iterator();
		while(values.hasNext()) {
			TimetableLesson lesson = values.next();
			if(lesson.isBreak && pointer == position)
				return -1;
			else if(lesson.isBreak)
				breaks++;
			else if(position == pointer)
				return (byte) (position-breaks);
			pointer++;
		}
		return -1;
	}
	
	public JSONArray json() throws JSONException {
		JSONArray array = new JSONArray();
		Iterator<TimetableLesson> iterator = iterator();
		while(iterator.hasNext()) {
			array.put(iterator.next().json());
		}
		return array;
	}
}
