package tobiass.rooster.timetable;

import org.json.JSONException;
import org.json.JSONObject;

import tobiass.rooster.api.JSONWriter;
import tobiass.rooster.storage.DB;
import android.content.ContentValues;

public class TimetableLesson {
	private static String ISBREAK = "a";
	private static String STARTHOUR = "b";
	private static String STARTMINUTE = "c";
	private static String LENGTH = "d";
	public boolean isBreak;
	public byte startHour;
	public byte startMinute;
	public int length;
	public int startStamp;
	public TimetableLesson(boolean isBreak, byte startHour, byte startMinute, int length) {
		this.isBreak = isBreak;
		this.startHour = startHour;
		this.startMinute = startMinute;
		this.length = length;
		this.startStamp = startHour*60 + startMinute;
	}
	
	public TimetableLesson(JSONObject obj) {
		isBreak = obj.optBoolean(ISBREAK);
		startHour = (byte) obj.optInt(STARTHOUR);
		startMinute = (byte) obj.optInt(STARTMINUTE);
		length = obj.optInt(LENGTH);
		startStamp = startHour*60 + startMinute;
	}
	
	public JSONObject json() throws JSONException {
		 return new JSONWriter(new JSONObject())
		 .put(ISBREAK, isBreak)
		 .put(STARTHOUR, startHour)
		 .put(STARTMINUTE, startMinute)
		 .put(LENGTH, length)
		 .getJSONObject();
	}
	
	public ContentValues getContentValues() {
		ContentValues v = new ContentValues(5);
		v.put(DB.CIS_BREAK, isBreak);
		v.put(DB.CSTARTHOUR, startHour);
		v.put(DB.CSTARTMINUTE, startMinute);
		v.put(DB.CLENGTH, length);
		return v;
	}
	
	public String getStartTime() {
		return String.format("%02d:%02d", (int) startHour, (int) startMinute);
	}
	
	public String getEndTime() {
		int hours = (int) Math.floor(length/60);
		int mins = (length-(hours*60))+(int) startMinute;
		if(mins > 59) {
			hours++;
			mins -= 60;
		}
		hours += startHour;
		
		// unlikely, but still..
		if(hours > 23) {
			hours -= 24;
		}
		return String.format("%02d:%02d", hours, mins);
	}
	
	public int[] getEndTimeNumeric() {
		int hours = (int) Math.floor(length/60);
		int mins = (length-(hours*60))+(int) startMinute;
		if(mins > 59) {
			hours++;
			mins -= 60;
		}
		hours += startHour;
		
		// unlikely, but still..
		if(hours > 23) {
			hours -= 24;
		}
		return new int[] {hours, mins};
	}
}