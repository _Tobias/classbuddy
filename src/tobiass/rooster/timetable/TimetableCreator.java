package tobiass.rooster.timetable;

import net.simonvt.numberpicker.NumberPicker;
import tobiass.rooster.R;
import tobiass.rooster.misc.ErrorDismissWatcher;
import tobiass.rooster.misc.Static;
import tobiass.rooster.storage.ObjectStore;
import tobiass.rooster.storage.Timetables;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;

public class TimetableCreator extends SherlockActivity {
	private Timetable mTimetable;
	private String[] mPresetNames;
	private Timetables mDb;
	private TimetableAdapter mAdapter;
	private EditText presetName;
	private Button deleteBottom;
	private static final String TIMETABLE = "a";
	private static final String NAME = "b";
	private static final int OSI = 2;
	private ListView list;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_timetablecreator);
		
		presetName = (EditText) findViewById(R.id.input);
		presetName.addTextChangedListener(new ErrorDismissWatcher(presetName));
		
		deleteBottom = (Button) findViewById(R.id.deleteBottomLesson);
		list = (ListView) findViewById(R.id.list);
		
		if(savedInstanceState != null) {
			presetName.setText(savedInstanceState.getString(NAME));
			int pointer = savedInstanceState.getInt(TIMETABLE);
			mTimetable = (Timetable) ObjectStore.get(OSI, pointer);
			if(pointer != 0)
				ObjectStore.remove(OSI, pointer);
		}
		if(mTimetable == null)
			mTimetable = new Timetable();
		
		mAdapter = new TimetableAdapter(this, mTimetable);
		list.setAdapter(mAdapter);
		
		ActionBar bar = getSupportActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		
		if(mTimetable.size() > 0)
			deleteBottom.setVisibility(View.VISIBLE);
	}
	
	public void onSaveInstanceState(Bundle out) {
		out.putInt(TIMETABLE, ObjectStore.store(OSI, mTimetable));
		out.putString(NAME, presetName.getText().toString());
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			onBackPressed();
		}
		return true;
	}
	
	public void onBackPressed() {
		if(mTimetable.size() != 0) {
			new AlertDialog.Builder(this)
			.setMessage(R.string.timetable_discard_warning)
			.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			})
			.setNegativeButton(R.string.cancel, null)
			.show();
		}
		else {
			finish();
		}
		
	}
	
	public void onResume() {
		super.onResume();
		mDb = new Timetables(this);
		mPresetNames = mDb.getPresetNames();
	}
	
	public void onPause() {
		mDb.close();
		super.onPause();
	}
	
	public void deleteBottomLesson(View v) {
		mAdapter.remove(mTimetable.get(mTimetable.size()-1));
		if(mTimetable.size() == 0) {
			v.setVisibility(View.INVISIBLE);
		}
	}
	
	public void addLesson(View v) {
		add(true);
	}
	
	public void addBreak(View v) {
		add(false);
	}
	
	private static String defaultPickerValue = "50";
	private static String[] durationPickerValues;
	public void add(final boolean isLesson) {
		final ViewGroup root = (ViewGroup) View.inflate(TimetableCreator.this, R.layout.dialog_add_lesson, null);
		final TimePicker timePicker = (TimePicker) root.findViewById(R.id.startTimePicker);
		final NumberPicker duration = (NumberPicker) root.findViewById(R.id.durationPicker);
		timePicker.setIs24HourView(true);
		timePicker.setCurrentHour(8);
		timePicker.setCurrentMinute(20);
		
		if(durationPickerValues == null)
			durationPickerValues = getResources().getStringArray(R.array.duration_picker_values);
		
		duration.setDisplayedValues(durationPickerValues);
		duration.setMinValue(0);
		duration.setMaxValue(durationPickerValues.length-1);
		
		String searchValue = defaultPickerValue;
		if(mTimetable.size() > 0) {
			TimetableLesson lesson = mTimetable.get(mTimetable.size()-1);
			int[] endTime = lesson.getEndTimeNumeric();
			timePicker.setCurrentHour(Integer.valueOf(endTime[0]));
			timePicker.setCurrentMinute(Integer.valueOf(endTime[1]));
			searchValue = String.valueOf(lesson.length);
		}
		int p = 0;
		for(String d : durationPickerValues) {
			if(searchValue.equals(d)) {
				duration.setValue(p);
				break;
			}
			p++;
		}
		
		final AlertDialog dialog = new AlertDialog.Builder(TimetableCreator.this)
		.setView(root)
		.setPositiveButton(R.string.timetable_lesson_save, null)
		.setNegativeButton(R.string.cancel, null)
		.show();
		
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
				mAdapter.add(new TimetableLesson(!isLesson, timePicker.getCurrentHour().byteValue(), timePicker.getCurrentMinute().byteValue(), Integer.valueOf(durationPickerValues[duration.getValue()])));
				deleteBottom.setVisibility(View.VISIBLE);
			}
		});
		dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}
	
	private MenuItem mAddButton;
	public boolean onCreateOptionsMenu(Menu menu) {
		mAddButton = menu.add(R.string.timetable_add_preset);
		mAddButton.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		mAddButton.setIcon(R.drawable.ic_action_accept);
		mAddButton.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				
				String name = presetName.getText().toString().trim();
				if(Static.contains(mPresetNames, name))
					presetName.setError(getText(R.string.name_duplicate));
				else if(name.length() == 0)
					presetName.setError(getText(R.string.name_too_short));
				else if(mTimetable.size() == 0)
					Toast.makeText(TimetableCreator.this, R.string.timetable_no_lessons, Toast.LENGTH_SHORT).show();
				else {
					mDb.insertTimetable(mTimetable, name);
					finish();
				}
				return true;
			}
		});
		return true;
	}
}
