package tobiass.rooster.misc;

import java.io.InputStream;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import tobiass.rooster.R;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class Static {
	public static String join(List<String> list, String conjunction) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (String item : list) {
			if (first) {
				first = false;
			} else {
				sb.append(conjunction);
			}
			sb.append(item);
		}
		return sb.toString();
	}
	
	public static String join(String[] list, String conjunction) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (String item : list) {
			if (first) {
				first = false;
			} else {
				sb.append(conjunction);
			}
			sb.append(item);
		}
		return sb.toString();
	}
	
	public static String getBetween(String before, String after, String input) {
		int a = input.indexOf(before)+before.length();
		int b = input.indexOf(after, a+1);
		return a > -1 && b > -1 ? input.substring(a, b) : null;
	}
	
	
	public static String convertStream(InputStream is) {
		return convertStream(is, null);
	}
	
	public static String convertStream(InputStream is, String charset) {
	    Scanner s = new Scanner(is, charset == null ? Const.DEFAULT_CHARSET : charset);
	    s.useDelimiter("\\A");
	    String a = s.hasNext() ? s.next() : "";
	    s.close();
	    return a;
	}
	
	public static int getHour() {
		return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
	}
	
	public static int getRealDayOfWeek() {
		return Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
	}
	
	public static int getDayOfWeek() {
		int d = getRealDayOfWeek()-2; // monday = 0, sunday = -1, friday = 4, saturday = 5
		if(getHour() >= 16) {
			d++;
		}
		if(d > 4 || d < 0) {
			d = 0;
		}
		return d;
	}
	
	public static void openPlayStore(Context context) {
		Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			context.startActivity(goToMarket);
		} catch (ActivityNotFoundException e) {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
					.parse("http://play.google.com/store/apps/details?id="
							+ context.getPackageName())));
		}
	}
	
	public static String handleException(Context context, Exception e) {
		if(e instanceof UnknownHostException) {
			return context.getString(R.string.error_no_inet);
		}
		return e.getClass().getName();
	}
	
	public static String md5toString(byte[] hash) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
			if ((0xff & hash[i]) < 0x10) {
				hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
			} else {
				hexString.append(Integer.toHexString(0xFF & hash[i]));
			}
		}
		return hexString.toString();
	}
	
	public static boolean contains(String[] arr, String target) {
		for(String s : arr) {
			if(target.equals(s))
				return true;
		}
		return false;
	}
	
	public static int getNeutralTimestamp(Calendar cal) {
		int ts = (int) Math.floor(cal.getTimeInMillis()/1000);
		ts -= cal.get(Calendar.HOUR_OF_DAY)*3600;
		ts -= cal.get(Calendar.MINUTE)*60;
		ts -= cal.get(Calendar.SECOND);
		return ts;
	}
	
	public static int getNeutralTimestamp() {
		return getNeutralTimestamp(Calendar.getInstance());
	}
	
	public static void dockKeyboard(EditText field) {
		((InputMethodManager)field.getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(field.getWindowToken(), 0);
		field.clearFocus();
	}
	
	public static final Comparator<String> stringComparator = new Comparator<String>() {
		public int compare(String lhs, String rhs) {
			return lhs == null || rhs == null ? 0 : lhs.compareToIgnoreCase(rhs);
		}
	};
	
	public static String stampToString(int stamp) {
<<<<<<< HEAD
		Date d = new Date((long)stamp*1000);
		return parseFormat.format(d);
	}
	
	private static SimpleDateFormat parseFormat = new SimpleDateFormat("EEEE d MMMM yyyy", Locale.getDefault());
	public static String dateToString(Date d) {
		return parseFormat.format(d);
=======
		SimpleDateFormat parseFormat = new SimpleDateFormat("EEEE d MMMM yyyy", Locale.getDefault());
		Date d = new Date((long)stamp*1000);
		return parseFormat.format(d);
>>>>>>> Some homework stuff updated
	}
	
	public static void shareText(Context context, String text, String subject) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		intent.putExtra(Intent.EXTRA_TEXT, text);
		if(subject != null)
			intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		context.startActivity(Intent.createChooser(intent, context.getText(R.string.share)));
	}
}