package tobiass.rooster.misc;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class ErrorDismissWatcher implements TextWatcher {
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	public void afterTextChanged(Editable s) {}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		mField.setError(null);
	}
	
	private EditText mField;
	public ErrorDismissWatcher(EditText field) {
		mField = field;
	}
}
