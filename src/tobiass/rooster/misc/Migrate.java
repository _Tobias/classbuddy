package tobiass.rooster.misc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import tobiass.rooster.api.Friend;
import tobiass.rooster.api.School;
import tobiass.rooster.storage.DB;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

public class Migrate {
	public static void database(SQLiteDatabase db, Context context) {
		File old = context.getFileStreamPath(Const.LFRIENDSFILE);
		if(old.exists()) {
			try {
				FileInputStream fis = context.openFileInput(Const.LFRIENDSFILE);
				String s = Static.convertStream(fis);
				fis.close();
				String[] lines = s.split("\n");
				for(String l : lines) {
					if(l.length() == 0) {
						continue;
					}
					
					String[] sp = l.split(";");
					if(sp.length < 4) {
						continue;
					}
					int school = 0;
					try {
						school = Integer.valueOf(sp[1]);
					}
					catch(NumberFormatException ex) {
						continue;
					}
					if(sp.length == 4) {
						Friend f = new Friend();
						f.school = school;
						f.name = sp[3];
						f._class = sp[2];
						f.type = Const.TYPE_CLASS;
						db.insert(DB.TABLE_NAME_FRIENDS, null, f.getContentValues());
					}
					else if(sp.length == 5) {
						Friend f = new Friend();
						f.school = school;
						f.name = sp[3];
						f.department = sp[2];
						try {
							f.student_id = Integer.valueOf(sp[4]);
						}
						catch(NumberFormatException ex) {
							continue;
						}
						f.type = Const.TYPE_STUDENT;
						db.insert(DB.TABLE_NAME_FRIENDS, null, f.getContentValues());
					}
				}
			}
			catch (FileNotFoundException e) {}
			catch (IOException e) {}
			old.delete();
		}
	}
	
	public static int GENERAL_VERSION = 3;
	public static void general(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		int oldVersion = prefs.getInt(Const.PREF_GENERALVERSION, -1);
		if(oldVersion != GENERAL_VERSION) {
			prefs.edit().putInt(Const.PREF_GENERALVERSION, GENERAL_VERSION).commit();
			// migrating from <=2.0.7 to >=2.0.8
			if(oldVersion == -1) {
				prefs.edit().remove(Const.SCHOOL).remove(Const.LPREF_SCHOOL).remove(Const.LPREF_NOTIFICATIONS).putBoolean(Const.PREF_NOTIFICATIONS, prefs.getInt(Const.LPREF_NOTIFICATIONS, 0) != 0).commit();
				School.clearAllCache(context);
			}
			if(oldVersion < 3)
				prefs.edit().putString(Const.PREF_THEME, "#A6CFA1").commit();
		}
	}
}
