package tobiass.rooster.misc;

import tobiass.rooster.R;

import com.jfeinstein.jazzyviewpager.JazzyViewPager.TransitionEffect;

public class Const {
	public static final byte TYPE_CLASS = 1;
	public static final byte TYPE_STUDENT = 2;
	public static final byte TYPE_TEACHER = 4;
	public static final byte TYPE_ME = 8;
	public static final byte TYPE_CLASSROOM = 16;
	public static final byte OBJECTSTORE = 1;
	public static final int NOTIFICATION_ID_1 = R.id.teacher_spinner;
	public static final int NOTIFICATION_ID_2 = R.id.stack;
	
	// SECOND
	public static final int EXPIRY_CACHE = 20*60;
	
	// MILLISECOND
	public static final int DEFAULT_INTERVAL = 20*60000;
	public static final long EXPIRY_SCHOOL_LIST = 1000*3600*48; // 48 hours
	
	public static final int CHANGELOG_VERSION = 1;
	
	public static final String TAG = "ClassBuddy";
	public static final String NOTIFICATION_CLICK = "tobiass.rooster.NOTIFICATION_CLICK";
	public static final String SCHEDULE = "tobiass.rooster.SCHEDULE";
	public static final String SCHEDULE_OBJECT_POINTER = "tobiass.rooster.SCHEDULE_OBJECT_POINTER";
	public static final String FRIEND_OBJECT_POINTER = "tobiass.rooster.FRIEND_OBJECT_POINTER";
	public static final String SCHEDULE_CHANGES = "tobiass.rooster.SCHEDULE_CHANGES";
	public static final String RESTART_PARENT = "tobiass.rooster.RESTART_PARENT";
	public static final String SHOW_HOME = "tobiass.rooster.SHOW_HOME";
	public static final String TAB = "tobiass.rooster.TAB";
	public static final String RETURN_SUBJECT = "tobiass.rooster.RETURN_SUBJECT";
	public static final String SUBJECT = "tobiass.rooster.SUBJECT";
	public static final String FINISH_AFTER_LOAD = "tobiass.rooster.FINISH_AFTER_LOAD";
	public static final String EDIT_HOMEWORK = "tobiass.rooster.EDIT_HOMEWORK";
	
	public static final String DEFAULT_CHARSET = "Windows-1252"; // GET YOUR SHIT TOGETHER GEPRO
	public static final String UTF8_CHARSET = "UTF-8";
	
	public static final String HASHING_ALGORITHM = "MD5";
	
	public static final TransitionEffect TRANSITION_EFFECT = TransitionEffect.Tablet;
	
	public static final String ID = "a";
	public static final String TYPE = "b";
	public static final String STUDENT_ID = "c";
	public static final String SCHOOL = "d";
	public static final String ABBREVIATION = "e";
	public static final String NAME = "f";
	public static final String DEPARTMENT = "g";
	public static final String CLASS = "h";
	public static final String CLASSROOM = "i";
	
	public static final String CONTINUE = "j";
	
	public static final String PREF_CLASSGUIDE = "a";
	public static final String PREF_NOTIFICATIONS = "b";
	public static final String PREF_SCHOOL = "c";
	public static final String PREF_GENERALVERSION = "e";
	public static final String PREF_CHANGELOGVERSION = "f";
	public static final String PREF_TIMETABLE = "g";
	public static final String PREF_THEME = "h";
	public static final String PREF_SUBJECTS = "i";
	
	// legacy
	public static final String LPREF_NOTIFICATIONS = "interval";
	public static final String LPREF_SCHOOL = "school";
	public static final String LFRIENDSFILE = "vrienden.txt";
}
