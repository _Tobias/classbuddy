package tobiass.rooster.misc;

/**
 * Helper class for showing Toast messages from Thread's other than the main UI thread.
 * Note that makeText doesn't return an object of type Toast, the Toast is shown right
 * away.
 */

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

public final class CrossToast {
	private static Context context = null;
	private static String text = null;
	private static int duration = Toast.LENGTH_SHORT;
	private static Runnable makeText = new Runnable() {
		public void run() {
			Toast.makeText(context, text, duration).show();
		}
	};
	
	public static void makeText(Activity c, String t, int d) {
		context = c;
		text = t;
		duration = d;
		c.runOnUiThread(makeText);
	}
	
	public static void makeText(Activity c, int t, int d) {
		makeText(c, c.getString(t), d);
	}
}
