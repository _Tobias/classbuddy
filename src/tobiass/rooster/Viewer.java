package tobiass.rooster;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import tobiass.rooster.api.Day;
import tobiass.rooster.api.Friend;
import tobiass.rooster.api.Hour;
import tobiass.rooster.api.Lesson;
import tobiass.rooster.api.Request;
import tobiass.rooster.api.Schedule;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.CrossToast;
import tobiass.rooster.misc.Static;
import tobiass.rooster.storage.Friends;
import tobiass.rooster.storage.ObjectStore.ObjectStoreWrapper;
import tobiass.rooster.storage.ScheduleCache;
import tobiass.widget.TViewPager;
import uk.co.senab.actionbarpulltorefresh.extras.actionbarsherlock.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.Options;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import uk.co.senab.actionbarpulltorefresh.library.viewdelegates.ViewDelegate;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.actionbarsherlock.view.Window;

public class Viewer extends SherlockFragmentActivity implements OnRefreshListener, ViewDelegate, OnClickListener {
	public PagerAdapter pagerAdapter;
	public ViewPager viewPager;
	public PagerTitleStrip viewPagerTitleStrip;
	public TViewPager viewPagerHandler;
	private Schedule schedule;
	private boolean restartParent;
	private PullToRefreshLayout ptr;
	public int toTab = -1;
	private boolean changes;
	private boolean finishAfterLoad;
	private Friend me;
	public static final int OSI = 1;
	
	private Animation noAlpha, fadeIn;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_viewer);
		
		ptr = (PullToRefreshLayout) findViewById(R.id.ptr);
        ActionBarPullToRefresh.from(this)
        .allChildrenArePullable()
        .options(Options.create().refreshOnUp(true)
        		.scrollDistance(0.3f)
        		.noMinimize()
        		.build())
        .useViewDelegate(ViewPager.class, this)
        .listener(this)
        .setup(ptr);
		
		viewPagerHandler = new TViewPager(findViewById(R.id.pager), Const.TRANSITION_EFFECT);
		viewPager = viewPagerHandler.getViewPager();
		viewPagerTitleStrip = (PagerTitleStrip) viewPager.getChildAt(0);
		noAlpha = new AlphaAnimation(0, 0);
		noAlpha.setDuration(0);
		noAlpha.setFillAfter(true);
		fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
		fadeIn.setFillAfter(true);
		
		final ActionBar bar = getSupportActionBar();
		
		setSupportProgressBarIndeterminateVisibility(false);
		bar.setDisplayHomeAsUpEnabled(true);
		
		Intent starter = getIntent();
		
		finishAfterLoad = starter.getBooleanExtra(Const.FINISH_AFTER_LOAD, false);
		
		if(savedInstanceState != null) {
			restartParent = savedInstanceState.getBoolean(Const.RESTART_PARENT);
			changes = savedInstanceState.getBoolean(Const.SCHEDULE_CHANGES);
			toTab = savedInstanceState.getInt(Const.TAB, -1);
		}
		
		schedule = (Schedule) getLastCustomNonConfigurationInstance();
		
		if(schedule != null) {
			me = schedule.friend;
			load();
		}
		else {
			ObjectStoreWrapper w = new ObjectStoreWrapper(OSI);
			
			boolean loadSchedule = starter.getBooleanExtra(Const.SCHEDULE, false);
			boolean loadChanges = starter.getBooleanExtra(Const.SCHEDULE_CHANGES, false) || starter.getBooleanExtra(Const.NOTIFICATION_CLICK, false);
			int loadPointer = starter.getIntExtra(Const.FRIEND_OBJECT_POINTER, -1);
			restartParent = starter.getBooleanExtra(Const.RESTART_PARENT, false) || loadSchedule || loadChanges;
			if(loadSchedule || loadChanges) {
				if(loadPointer != -1) {
					me = (Friend) w.get(loadPointer);
				}
				else {
					Friends f = new Friends(this);
					me = f.getMe();
					f.close();
					
					if(me == null) {
						onBackPressed();
						return;
					}
				}
				changes = loadChanges;

				new LoadAsync().execute();
			}
		}
	}
	
	private void load() {
		ptr.setRefreshComplete();
		supportInvalidateOptionsMenu();
		ActionBar bar = getSupportActionBar();
		bar.setTitle(schedule.friend.toString());
		bar.setSubtitle(schedule.lastMod);
		
		pagerAdapter = new DayFragmentAdapter(getSupportFragmentManager());
		if(toTab == -1) {
			toTab = Static.getDayOfWeek();
		}
		viewPagerTitleStrip.startAnimation(fadeIn);
		viewPager.setAdapter(pagerAdapter);
		viewPager.setCurrentItem(toTab);
		toTab = -1;
	}
	
	private void setError(String error) {
		ViewSwitcher l = (ViewSwitcher) findViewById(R.id.root);
		
		if(l != null) {
			l.setDisplayedChild(1);
			TextView t = (TextView) l.getChildAt(1);
			t.setText(error);
		}
		if(ptr != null)
			ptr.setRefreshComplete();
	}
	
	private class LoadAsync extends AsyncTask<Boolean, Void, String> {
		protected void onPreExecute() {
			schedule = null;
			if(viewPager.getAdapter() != null) {
				toTab = viewPager.getCurrentItem();
			}
			viewPager.setAdapter(null);
			viewPagerTitleStrip.startAnimation(noAlpha);
			ptr.setRefreshing(true);
			supportInvalidateOptionsMenu();
		}

		protected String doInBackground(Boolean... params) {
			ScheduleCache c = null;
			
			// If called after pull-to-refresh, the params array has length 1,
			// otherwise length 0. Therefore, we should use the cache when
			// possible if length == 0
			if(params.length == 0 && me.id != -1) {
				c = new ScheduleCache(Viewer.this);
				try {
					schedule = c.getSchedule(me.id, changes, false);
					if(schedule != null) {
						c.close();
						return null;
					}
				}
				catch (Exception e) {}
				c.close();
			}
			
			// No cache available, continue by doing a request.
			final Request req = new Request();
			req.setFriend(me);
			req.changes = changes;
			req.async = false;
			req.run(Viewer.this);
			
			// Request failed?
			if(req.error != null) {
				
				// Indicating that me is currently a Friend
				if(me.id != -1) {
					// Get older cache
					try {
						c = new ScheduleCache(Viewer.this);
						schedule = c.getSchedule(me.id, req.changes, true);
						c.close();
						if(schedule != null) {
							// Warn the user that the data is outdated
							CrossToast.makeText(Viewer.this, R.string.warn_data_old, Toast.LENGTH_SHORT);
							return null;
						}
					} catch (Exception e) {}
				}
				return req.error;
			}
			schedule = req.result;
			
			// Indicating that me is currently a Friend
			if(me.id != -1) {
				// Insert cache entry into the database
				c = new ScheduleCache(Viewer.this);
				try {
					c.insert(schedule);
				}
				catch (Exception e) {}
				c.close();
			}
			return null;
		}
		
		protected void onPostExecute(String error) {
			if(schedule != null) {
				if(finishAfterLoad) {
					ObjectStoreWrapper w = new ObjectStoreWrapper(OSI);
					setResult(RESULT_OK, getIntent().putExtra(Const.SCHEDULE, w.store(schedule)));
					finish();
				}
				else
					load();
			}
			else
				setError(error != null ? error : getString(R.string.error_no_inet));
		}
	};
	
	protected void onSaveInstanceState(Bundle outState) {		
		if(viewPager != null) {
			outState.putInt(Const.TAB, viewPager.getCurrentItem());
		}
		outState.putBoolean(Const.RESTART_PARENT, restartParent);
		outState.putBoolean(Const.SCHEDULE_CHANGES, changes);
	}
	
	public Object onRetainCustomNonConfigurationInstance() {
		return schedule;
	}
	
	public Day getDay(int day) {
		return schedule.schedule.get(day);
	}
	
	public int getType() {
		return schedule.friend.type;
	}
	
	private SparseArray<ScrollView> scrollViews = new SparseArray<ScrollView>(5);
	public void registerScrollView(int day, final ScrollView scrollView) {
		scrollViews.put(day, scrollView);
	}
	
	public void unregisterScrollView(int day) {
		scrollViews.remove(day);
	}
	
	public boolean isReadyForPull(View view, float x, float y) {
		ScrollView v = scrollViews.get(viewPager.getCurrentItem());
		return v != null && v.getScrollY() == 0;
	}
	
	private class DayFragmentAdapter extends FragmentPagerAdapter {
		public DayFragmentAdapter(FragmentManager fm) {
			super(fm);			
			days = getResources().getStringArray(R.array.days);
		}
		
		public Object instantiateItem(ViewGroup container, final int position) {
		    Object obj = super.instantiateItem(container, position);
		    viewPagerHandler.setObjectForPosition(obj, position);
		    return obj;
		}
		
		private String[] days;
		
		public Fragment getItem(int arg0) {
			DayFragment f = new DayFragment();
			Bundle b = new Bundle();
			b.putInt("day", arg0);
			f.setArguments(b);
			return f;
		}
		
		public CharSequence getPageTitle(int pos) {
			if(pos < 5) {
				return days[pos].toUpperCase();
			}
			else {
				return getString(R.string.settings).toUpperCase();
			}
		}

		public int getCount() {
			return 5;
		}
	}
	
	public static class DayFragment extends SherlockFragment {
		private Viewer viewer;
		private int dayNo;
		private TableLayout table;
		private ViewSwitcher switcher;
		private BuildViewHierarchy loader;
		private static String es = "";
		
		private class BuildViewHierarchy extends AsyncTask<Void, Void, List<View>> {
			
			public void onPreExecute() {
				switcher.setDisplayedChild(0);
			}
			
			private boolean hideClusters = true;
			protected List<View> doInBackground(Void... params) {
				List<View> table = new ArrayList<View>();
				
		        Day day = viewer.getDay(dayNo);
				
				int highlight = getResources().getColor(R.color.highlight_lesson);
				
				Iterator<Hour> dagIterator = day.iterator();
				int x = 1;
				
				final Viewer c = viewer;
				
				switch(viewer.getType()) {
				case Const.TYPE_CLASS:
				case Const.TYPE_STUDENT:
					table.add(getRow(c, new String[]{es, getString(R.string.classroom), getString(R.string.teacher), getString(R.string.subject)}));
					break;
				case Const.TYPE_TEACHER:
					table.add(getRow(c, new String[]{es, getString(R.string.classroom), getString(R.string._class), getString(R.string.subject)}));
					break;
				case Const.TYPE_CLASSROOM:
					table.add(getRow(c, new String[]{es, getString(R.string._class), getString(R.string.teacher), getString(R.string.subject)}));
					break;
				}
				
				while(dagIterator.hasNext()) {
					List<Lesson> hour = dagIterator.next();
					if(hour.size() == 0 || (hour.get(0).free && !hour.get(0).updated && hour.size() == 1)) {
						TableRow r = new TableRow(c);
						r.addView(getTextView(c, String.valueOf(x)));
						table.add(r);
					}
					else {
						Iterator<Lesson> lessons = hour.iterator();
						boolean first = true;
						while(lessons.hasNext()) {
							String ind = es;
							if(first) {
								first = false;
								ind = String.valueOf(x);
							}
							Lesson a = lessons.next();
							if(!a.free) {
								if(a.cluster == null)
									a.cluster = es;
								else
									hideClusters = false;
								
								switch(viewer.getType()) {
								case Const.TYPE_CLASS:
									table.add(getRow(c, new String[]{ind, a.classroom, a.teacher, a.subject, a.cluster}));
									break;
								case Const.TYPE_STUDENT:
									table.add(getRow(c, new String[]{ind, a.classroom, a.teacher, a.subject, a.cluster}));
									break;
								case Const.TYPE_TEACHER:
									table.add(getRow(c, new String[]{ind, a.classroom, a._class, a.subject, a.cluster}));
									break;
								case Const.TYPE_CLASSROOM:
									table.add(getRow(c, new String[]{ind, a._class, a.teacher, a.subject, a.cluster}));
									break;
								}
							}
							else if(!(a.free && !a.updated)) {
								table.add(getRow(c, new String[]{ind, getString(R.string.free).toLowerCase()}));
							}
							if(a.updated) {
								TableRow row = (TableRow) table.get(table.size()-1);
								for(int i = 0; i < row.getChildCount(); i++) {
									if(i == 0) continue;
									((TextView) row.getChildAt(i)).setTextColor(highlight);
								}
							}
						}
					}
					x++;
				}
				return table;
			}
			
			public void onPostExecute(List<View> result) {
				for(int i = 0; i < result.size(); i++) {
					TableRow row = (TableRow) result.get(i);
					if(hideClusters && row.getChildCount() == 5)
						row.removeViewAt(row.getChildCount()-1);
					table.addView(row);
				}
				
				table.setVisibility(View.VISIBLE);
				
				switcher.setDisplayedChild(1);
			}
		}
		
		/**
		 * Build view hierarchy. On a seperate thread, how cool is that!
		 */
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			final View v = inflater.inflate(R.layout.frag_day, null);
			table = (TableLayout) v.findViewById(R.id.table);
			
			// Switcher is used for switching to a layout that shows a loading
			// indicator, this is shown while building the view stack.
			switcher = (ViewSwitcher) v.findViewById(R.id.viewSwitcher);
			viewer = (Viewer) getActivity();
			dayNo = getArguments().getInt("day");
			
			// Registering scroll view in the activity so PullToRefresh will
			// know when it's scrolled all the way up.
			viewer.registerScrollView(dayNo, (ScrollView) v.findViewById(R.id.scrollView));
			
			loader = new BuildViewHierarchy();
			loader.execute();
			return v;
		}
		
		public void onDestroyView() {
			viewer.unregisterScrollView(dayNo);
			loader.cancel(false);
			super.onDestroyView();
		}
		
		private TextView getTextView(Context c, String text) {
			TextView v = (TextView) getActivity().getLayoutInflater().inflate(R.layout.row_lesson, null);
			v.setText(text);
			return v;
		}
		
		private TableRow getRow(Context c, String[] texts) {
			TableRow row = new TableRow(c);
			for(String text : texts) {
				TextView v = getTextView(c, text == null || text.toString().equals("null") ? "?" : text);
				row.addView(v);
			}
			return row;
		}
	}

	
	public static void start(Context c, Friend s, boolean restartParent, boolean changes) {
		start(c, s, restartParent, changes, false);
	}
	
	public static void start(Context c, Friend s, boolean restartParent, boolean changes, boolean finishAfterLoad) {
		c.startActivity(startIntent(c, s, restartParent, changes, finishAfterLoad));
	}
	
	public static Intent startIntent(Context c, Friend s, boolean restartParent, boolean changes, boolean finishAfterLoad) {
		ObjectStoreWrapper w = new ObjectStoreWrapper(OSI);
		Intent i = new Intent(c, Viewer.class);
		i.putExtra(Const.FRIEND_OBJECT_POINTER, w.store(s));
		i.putExtra(changes ? Const.SCHEDULE_CHANGES : Const.SCHEDULE, true);
		i.putExtra(Const.RESTART_PARENT, restartParent);
		i.putExtra(Const.FINISH_AFTER_LOAD, finishAfterLoad);
		return i;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			onBackPressed();
		}
		return true;
	}
	
	public void onBackPressed() {
		finish();
		if(restartParent) {
			startActivity(new Intent(this, Main.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
		}
	}

	public void onRefreshStarted(View view) {
		new LoadAsync().execute(true);
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuItem item = menu.add(R.string.display);
		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		item.setIcon(R.drawable.ic_action_settings);
		item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				new AlertDialog.Builder(Viewer.this)
				.setSingleChoiceItems(R.array.mode_selection, changes ? 1 : 0, Viewer.this)
				.show();
				return true;
			}
		});
		return ptr != null && !ptr.isRefreshing();
	}

	public void onClick(DialogInterface dialog, int which) {
		if(changes == (which == 1)) {
			dialog.dismiss();
			return;
		}
		changes = which == 1;
		new LoadAsync().execute();
		dialog.dismiss();
	}
}
