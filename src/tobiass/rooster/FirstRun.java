package tobiass.rooster;

import tobiass.rooster.api.Friend;
import tobiass.rooster.api.School;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.Migrate;
import tobiass.rooster.storage.Friends;
import tobiass.rooster.storage.ScheduleCache;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.actionbarsherlock.view.Window;

public class FirstRun extends SherlockActivity {
	private SparseArray<String> schools = null;
	private int[] schoolKeys = null;
	private Spinner department = null;
	private Spinner _class = null;
	private Spinner teacher = null;
	private Spinner school_spinner = null;
	private EditText student_id = null;
	private RadioGroup profile_type = null;
	private ViewSwitcher rootView = null;
	
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_firstrun);
		
		Migrate.general(this);
		
		getSupportActionBar().setSubtitle(R.string.intro_name);
		if(getIntent().getBooleanExtra(Const.SHOW_HOME, false)) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
		
		rootView = (ViewSwitcher) ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
		department = (Spinner) findViewById(R.id.department_spinner);
		department.setAdapter(new UpdatableAdapter(this, null));
		_class = (Spinner) findViewById(R.id.class_spinner);
		_class.setAdapter(new UpdatableAdapter(this, null));
		teacher = (Spinner) findViewById(R.id.teacher_spinner);
		teacher.setAdapter(new UpdatableAdapter(this, null));
		school_spinner = (Spinner) findViewById(R.id.school_selector);
		student_id = (EditText) findViewById(R.id.student_id);
		
		student_id.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				student_id.setError(null);
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void afterTextChanged(Editable s) {}
		});
		
		setLoading(true);
		
		if(savedInstanceState != null) {
			_continue = savedInstanceState.getBoolean("continue");
			student_id.setText(savedInstanceState.getString("student_id"));
		}
		final ViewFlipper personal_flipper = (ViewFlipper) findViewById(R.id.personal_flipper);
		final View personal_fields;
		View _personal_fields = findViewById(R.id.personal_fields);
		profile_type = (RadioGroup) findViewById(R.id.profile_type);
		if(_personal_fields == null)
			personal_fields = profile_type;
		else
			personal_fields = _personal_fields;
		
		profile_type.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch(checkedId) {
				case R.id.profile_class:
					personal_flipper.setDisplayedChild(1);
					break;
				case R.id.profile_student:
					personal_flipper.setDisplayedChild(0);
					break;
				case R.id.profile_teacher:
					personal_flipper.setDisplayedChild(2);
					break;
				}
			}
		});
		
		school_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			
			// refreshFields does the same as fillPersonalStuff, except it also
			// updates the view state according to the savedInstanceState.
			private void refreshFields(School s) {
				if(savedInstanceState != null && savedInstanceState.containsKey(Const.DEPARTMENT) && savedInstanceState.containsKey(Const.CLASS)) {
					fillPersonalStuff(s, savedInstanceState.getInt(Const.DEPARTMENT, 0), savedInstanceState.getInt(Const.CLASS, 0), savedInstanceState.getInt(Const.ABBREVIATION, 0));
					profile_type.check(savedInstanceState.getInt(Const.TYPE));
					savedInstanceState.remove(Const.TYPE);
					savedInstanceState.remove(Const.DEPARTMENT);
					savedInstanceState.remove(Const.CLASS);
					savedInstanceState.remove(Const.ABBREVIATION);
				}
				else {
					fillPersonalStuff(s);
				}
			}
			
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				final int id = schoolKeys[arg2];
				if(!School.hasCached(id)) {
					setLoading(true);
					personal_fields.setVisibility(View.GONE);
					personal_flipper.setVisibility(View.GONE);
					new Thread(new Runnable() {
						public void run() {
							final School s = School.getSchool(FirstRun.this, id);
							runOnUiThread(new Runnable() {
								public void run() {
									setLoading(false);
									if(s.error > -1) {
										setError(s.error, true);
									}
									else {
										refreshFields(s);
										personal_fields.setVisibility(View.VISIBLE);
										personal_flipper.setVisibility(View.VISIBLE);
									}
								}
							});
						}
					}).start();
				}
				else {
					personal_fields.setVisibility(View.VISIBLE);
					personal_flipper.setVisibility(View.VISIBLE);
					refreshFields(School.getSchool(FirstRun.this, id));
					setLoading(false);
				}
			}
			
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
		
		final Runnable r = new Runnable() {
			public void run() {
				final String[] data = new String[schools.size()];
				schoolKeys = new int[schools.size()];
				for(int i = 0; i < schools.size(); i++) {
					schoolKeys[i] = schools.keyAt(i);
					data[i] = schools.get(schoolKeys[i]);
				}
				school_spinner.setAdapter(new ArrayAdapter<String>(FirstRun.this, android.R.layout.simple_list_item_1, data));
				rootView.setEnabled(true);
				findViewById(R.id.intro_root).setVisibility(View.VISIBLE);
				
				if(savedInstanceState != null) {
					school_spinner.setSelection(savedInstanceState.getInt("school_spinner", 0));
				}
			}
		};
		
		if(School.hasSchoolListCached()) {
			schools = School.getList(FirstRun.this);
			r.run();
		}
		else {
			new Thread(new Runnable() {
				public void run() {
					schools = School.getList(FirstRun.this);
					if(schools == null) {
						setError(R.string.error_schools_file, true);
					}
					else {
						runOnUiThread(r);
					}
				}
			}).start();
		}
	}
	
	private void fillPersonalStuff(final School s) {
		UpdatableAdapter departmentAdapter = (UpdatableAdapter) department.getAdapter();
		UpdatableAdapter classAdapter = (UpdatableAdapter) _class.getAdapter();
		UpdatableAdapter teacherAdapter = (UpdatableAdapter) teacher.getAdapter();
		
		departmentAdapter.updateData(s.getDepartments());
		classAdapter.updateData(s.getClasses());
		teacherAdapter.updateData(s.getTeachers());
	}
	
	private void fillPersonalStuff(final School s, int department_pos, int class_pos, int teacher_pos) {
		fillPersonalStuff(s);
		department.setSelection(department_pos);
		_class.setSelection(class_pos);
		teacher.setSelection(teacher_pos);
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			onBackPressed();
		}
		return true;
	}
	
	public void onSaveInstanceState(Bundle out) {
		out.putInt(Const.DEPARTMENT, department.getSelectedItemPosition());
		out.putInt(Const.CLASS, _class.getSelectedItemPosition());
		out.putInt(Const.ABBREVIATION, teacher.getSelectedItemPosition());
		out.putInt(Const.TYPE, profile_type.getCheckedRadioButtonId());
		out.putBoolean(Const.CONTINUE, _continue);
		out.putInt(Const.SCHOOL, school_spinner.getSelectedItemPosition());
		out.putString(Const.STUDENT_ID, student_id.getText().toString());
	}
	
	
	private void setLoading(boolean loading) {
		setSupportProgressBarIndeterminateVisibility(loading);
		rootView.setEnabled(loading);
		school_spinner.setEnabled(!loading);
		_continue = !loading;
		supportInvalidateOptionsMenu();
	}
	
	private boolean _continue = false;
	private void setError(final String error, final boolean severe) {
		runOnUiThread(new Runnable() {
			public void run() {
				if(severe) {
					setSupportProgressBarIndeterminateVisibility(false);
					getSupportActionBar().setSubtitle(null);
					_continue = false;
					supportInvalidateOptionsMenu();
					TextView t = (TextView) rootView.getChildAt(1);
					t.setText(error);
					rootView.setDisplayedChild(1);
				}
				else {
					setLoading(false);
					if(rootView.getDisplayedChild() == 0)
						student_id.setError(error);
					else
						Toast.makeText(FirstRun.this, error, Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private void setError(String error) {
		setError(error, false);
	}
	
	private void setError(int error) {
		setError(getString(error), false);
	}
	
	private void setError(int error, boolean severe) {
		setError(getString(error), severe);
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		if(_continue) {
			menu.add(R.string._continue)
			.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				public boolean onMenuItemClick(MenuItem item) {
					
					final Friend f = new Friend();
					f.school = schoolKeys[school_spinner.getSelectedItemPosition()];
					switch(profile_type.getCheckedRadioButtonId()) {
					case R.id.profile_class:
						f.type = Const.TYPE_CLASS;
						f._class = (String) _class.getSelectedItem();
						break;
					case R.id.profile_student:
						f.type = Const.TYPE_STUDENT;
						f.department = (String) department.getSelectedItem();
						try {
							f.student_id = Integer.parseInt(student_id.getText().toString());
						}
						catch(NumberFormatException e) {
							setError(R.string.invalid_number);
							return true;
						}
						
						setLoading(true);
						new Thread(new Runnable() {
							public void run() {
								final String res = f.addName(FirstRun.this);
								runOnUiThread(new Runnable() {
									public void run() {
										if(res == null) {
											submit(f);
										}
										else {
											setError(res);
										}
									}
								});
							}
						}).start();
						return true;
					case R.id.profile_teacher:
						f.type = Const.TYPE_TEACHER;
						f.abbreviation = (String) teacher.getSelectedItem();
						break;
					}
					submit(f);
					return true;
				}
			})
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		}
		return true;
	}
	
	private void submit(Friend f) {
		Friends x = new Friends(this);
		x.removeDuplicate(f);
		x.setMe(f);
		x.close();
		
		// much confuse, why remove?
		ScheduleCache cache = new ScheduleCache(this);
		cache.removeAll();
		cache.close();
		startActivity(new Intent(FirstRun.this, Main.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		finish();
	}
}
