package tobiass.rooster;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import tobiass.rooster.api.Friend;
import tobiass.rooster.fragments.FriendsFragment;
import tobiass.rooster.fragments.PersonalFragment;
import tobiass.rooster.fragments.ScheduleFragment;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.CrossToast;
import tobiass.rooster.misc.ErrorDismissWatcher;
import tobiass.rooster.misc.Migrate;
import tobiass.rooster.splash.Introduction;
import tobiass.rooster.splash.Licenses;
import tobiass.rooster.storage.Friends;
import tobiass.widget.TFragment;
import tobiass.widget.TFragment.FragmentLock;
import tobiass.widget.TViewPager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.actionbarsherlock.view.Window;

public class Main extends SherlockFragmentActivity implements FragmentLock {
	public PagerAdapter pagerAdapter;
	public ViewPager pager;
	public TViewPager pagerHandler;
	private TextView actionBarTitle;
	private String[] colorCodes;
	private Random random;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setSupportProgressBarIndeterminateVisibility(false);
		
		Migrate.general(this);
		
		Friends fr = new Friends(this);
		Friend me = fr.getMe();
		fr.close();
		
		if(me == null) {
			startActivity(new Intent(this, FirstRun.class));
			finish();
			return;
		}
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if(prefs.getInt(Const.PREF_CHANGELOGVERSION, -1) != Const.CHANGELOG_VERSION) {
			prefs.edit().putInt(Const.PREF_CHANGELOGVERSION, Const.CHANGELOG_VERSION).commit();
			startActivity(new Intent(this, Introduction.class));
		}
		
		setContentView(R.layout.activity_main);
		
	    actionBarTitle = (TextView) getWindow().findViewById(getResources().getIdentifier("action_bar_title", "id", "android"));
	    if(actionBarTitle == null)
	    	actionBarTitle = (TextView) getWindow().findViewById(R.id.abs__action_bar_title);
	    
	    colorCodes = getResources().getStringArray(R.array.theme_colors_codes);
	    loadTheme(prefs.getString(Const.PREF_THEME, null));
		
		Resources res = getResources();
		TFragment.setPadding(res.getDimensionPixelSize(R.dimen.activity_horizontal_margin), res.getDimensionPixelSize(R.dimen.activity_vertical_margin));
		
		pagerAdapter = new PagerAdapter(getSupportFragmentManager());
		
		int current_fragment = 0;
		if(savedInstanceState != null) {
			current_fragment = savedInstanceState.getInt(Const.TAB, 0);
			
			for(int i = 0; i < pagerAdapter.fragments.length; i++) {
				Bundle b = savedInstanceState.getBundle("fragment_"+i);
				if(b != null)
					pagerAdapter.fragments[i].setArguments(b);
			}
		}
		
		pagerHandler = new TViewPager(findViewById(R.id.pager), Const.TRANSITION_EFFECT);
		pager = pagerHandler.getViewPager();
		pager.setOffscreenPageLimit(pagerAdapter.fragments.length-1);
		
		//friend intent
		Intent starter = getIntent();
		String action = starter.getAction();
		if(action != null && action.equals(Intent.ACTION_VIEW)) {
			final String path = starter.getData().getPath();
			
			starter.setAction(Intent.ACTION_MAIN);
			starter.setData(null);
			
			if(path != null && path.length() > 1) {
				duplicates = new ArrayList<Friend>();
				
				new Import().execute(path.substring(1));
				return;
			}
			
		}
		
		loadFragments(current_fragment);
	}
	
	private void loadTheme(String color) {
		if(color != null) {
			ActionBar bar = getSupportActionBar();
			if(color.equals("d")) {
				if(random == null)
					random = new Random();
				color = colorCodes[random.nextInt(colorCodes.length-2)];
			}
			String[] split = color.split(";");
			Drawable d;
			int darkColor = getResources().getColor(R.color.actionbar_color_dark);
			int avgColor = 0;
			if(split.length > 1) {
				int[] colors = new int[split.length];
				for(int i = 0; i < split.length; i++) {
					colors[i] = split[i].length() > 0 ? Color.parseColor(split[i]) : darkColor;
					avgColor += colors[i];
				}
				avgColor /= split.length;
				d = new GradientDrawable(Orientation.LEFT_RIGHT, colors);
			}
			else {
				int solid = color.length() > 0 ? Color.parseColor(color) : darkColor;
				d = new ColorDrawable(solid);
				avgColor = solid;
			}
			bar.setBackgroundDrawable(d);
			if(actionBarTitle != null)
				actionBarTitle.setTextColor(-8323328 > avgColor ? Color.WHITE : Color.BLACK);
			bar.setDisplayShowTitleEnabled(false);
			bar.setDisplayShowTitleEnabled(true);
		}
	}
	
	private void loadFragments(int current_fragment) {
		pager.setAdapter(pagerAdapter);
		pager.setCurrentItem(current_fragment);
	}
	
	private class Import extends AsyncTask<String, Void, Void> {
		private ProgressDialog mProgressDialog;
		public void onPreExecute() {
			mProgressDialog = ProgressDialog.show(Main.this, getText(R.string.friend_importing), getString(R.string.friend_importing_progress, 0));
		}
		
		protected Void doInBackground(String... params) {
			Friends friends = new Friends(Main.this);
			Friend me = friends.getMe();
			
			for(String json : params[0].split("/")) {
				JSONObject obj = null;
				try {
					obj = new JSONObject(URLDecoder.decode(json, Const.UTF8_CHARSET));
				}
				catch (UnsupportedEncodingException e) {}
				catch (JSONException e) {
					CrossToast.makeText(Main.this, R.string.error_invalid_link, Toast.LENGTH_SHORT);
					importFailed++;
					continue;
				}
				
				final Friend f = new Friend(obj);
				
				if(me.equals(f)) {
					importMe = true;
					continue;
				}
				else if(friends.isDuplicate(f)) {
					importDuplicate++;
				}
				else {
					if(friends.isDuplicate(f.name)) {
						duplicates.add(f);
					}
					else {
						importAdded++;
						publishProgress();
						friends.addFriend(f);
					}
				}
			}
			
			friends.close();
			
			return null;
		}
		
		public void onProgressUpdate(Void... objects) {
			mProgressDialog.setMessage(getString(R.string.friend_importing_progress, importAdded));
		}
		
		public void onPostExecute(Void result) {
			mProgressDialog.cancel();
			handleDuplicates(new Friends(Main.this));
		}
	}
	
	private int importAdded;
	private int importFailed;
	private int importDuplicate;
	private boolean importMe;
	private ArrayList<Friend> duplicates;
	private Friend currentFriend;
	
	// fr will be closed in here.
	private void handleDuplicates(final Friends fr) {
		if(duplicates.size() == 0) {
			fr.close();
			loadFragments(1);
			showImportingResult();
			return;
		}
		
		final Iterator<Friend> iterator = duplicates.iterator();
		
		currentFriend = iterator.next();
		final View dialogView = getLayoutInflater().inflate(R.layout.dialog_nameask, null);
		final EditText nameAskField = (EditText) dialogView.findViewById(R.id.input);
		nameAskField.addTextChangedListener(new ErrorDismissWatcher(nameAskField));
		final TextView text = (TextView) dialogView.findViewById(R.id.text);
		text.setText(getString(R.string.name_friend_import, currentFriend.name));
		nameAskField.setText(currentFriend.name);
		
		final AlertDialog dialog = new AlertDialog.Builder(this)
		.setView(dialogView)
		.setPositiveButton(R.string.save_friend, null)
		.setNeutralButton(R.string.skip, null)
		.setNegativeButton(R.string.cancel, null)
		.show();
		
		dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String name = nameAskField.getText().toString().trim();
				if(name.length() == 0) {
					nameAskField.setError(getText(R.string.name_too_short));
					return;
				}
				if(fr.isDuplicate(name)) {
					nameAskField.setError(getText(R.string.friend_duplicate));
					return;
				}
				else {
					currentFriend.name = nameAskField.getText().toString().trim();
					importAdded++;
					fr.addFriend(currentFriend);
					dialog.getButton(AlertDialog.BUTTON_NEUTRAL).performClick();
				}
			}
		});
		
		dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(iterator.hasNext()) {
					currentFriend = iterator.next();
					text.setText(getString(R.string.name_friend_import, currentFriend.name));
					nameAskField.setText(currentFriend.name);
				}
				else {
					dialog.getButton(AlertDialog.BUTTON_NEGATIVE).performClick();
				}
			}
		});
		
		dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				fr.close();
				showImportingResult();
				dialog.cancel();
				loadFragments(1);
			}
		});
	}
	
	private void showImportingResult() {
		String s = "";
		if(importAdded > 0)
			s += getString(R.string.friend_result_added, importAdded)+"\n";
		if(importDuplicate > 0)
			s += getString(R.string.friend_result_duplicate, importDuplicate)+"\n";
		if(importFailed > 0)
			s += getString(R.string.friend_result_failed, importFailed)+"\n";
		if(importMe)
			s += getString(R.string.error_friend_me)+"\n";
		
		if(s.length() > 1)
			Toast.makeText(Main.this, getString(R.string.friend_import_completed, s.substring(0, s.length()-1)), Toast.LENGTH_SHORT).show();
	}
	
	public void refreshFriends() {
		((FriendsFragment) pagerAdapter.fragments[1]).refresh();
	}
	
	public void refreshFriends(Friends f) {
		((FriendsFragment) pagerAdapter.fragments[1]).refresh(f);
	}
	
	public void onSaveInstanceState(Bundle out) {
		if(pager != null) {
			out.putInt(Const.TAB, pager.getCurrentItem());
			for(int i = 0; i < pagerAdapter.fragments.length; i++) {
				if(pagerAdapter.fragments[i].isAdded()) {
					Bundle b = new Bundle();
					pagerAdapter.fragments[i].onSaveInstanceState(b);
					out.putBundle("fragment_"+i, b);
				}
			}
		}
	}
	
	private class PagerAdapter extends FragmentPagerAdapter {
		public PagerAdapter(FragmentManager fm) {
			super(fm);
		}
		
		public Object instantiateItem(ViewGroup container, final int position) {
		    Object obj = super.instantiateItem(container, position);
		    pagerHandler.setObjectForPosition(obj, position);
		    return obj;
		}
		
		public Fragment[] fragments = new Fragment[] { new PersonalFragment(), new FriendsFragment(), new ScheduleFragment() };
		private int[] titles = new int[] { R.string.personal, R.string.friends, R.string.schedule };
		public Fragment getItem(int arg0) {
			return fragments[arg0];
		}
		
		public CharSequence getPageTitle(int position) {
			return getString(titles[position]).toUpperCase(Locale.US);
		}

		public int getCount() {
			return fragments.length;
		}
	}
	
	private static void enableDisableView(View view, boolean enabled) {
		if(view != null) {
			view.setEnabled(enabled);
			if(view instanceof ViewGroup) {
				ViewGroup group = (ViewGroup) view;
				for (int idx = 0; idx < group.getChildCount(); idx++) {
					enableDisableView(group.getChildAt(idx), enabled);
				}
			}
		}
	}
	
	public void setLoading(boolean loading) {
		setSupportProgressBarIndeterminateVisibility(loading);
	}

	public void setLocked(boolean locked) {
		if(pager != null) {
			enableDisableView(pager, !locked);
			pagerHandler.setSwiping(!locked);
		}
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {		
		MenuItem licenses = menu.add(R.string.licenses);
		licenses.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		licenses.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				startActivity(new Intent(Main.this, Licenses.class));
				return true;
			}
		});
		
		MenuItem introduction = menu.add(R.string.introduction);
		introduction.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		introduction.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				startActivity(new Intent(Main.this, Introduction.class));
				return true;
			}
		});
		
		MenuItem theme = menu.add(R.string.select_theme);
		theme.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		theme.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				AlertDialog.Builder builder = new AlertDialog.Builder(Main.this);
				
				final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Main.this);
				String current = prefs.getString(Const.PREF_THEME, null);
				int position = 0;
				if(current != null) { 
					for(int i = 1; i < colorCodes.length; i++) {
						if(prefs.getString(Const.PREF_THEME, null).equals(colorCodes[i])) {
							position = i;
							break;
						}
					}
				}
				builder.setSingleChoiceItems(getResources().getStringArray(R.array.theme_colors), position, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						prefs.edit().putString(Const.PREF_THEME, colorCodes[which]).commit();
						loadTheme(colorCodes[which]);
						dialog.cancel();
					}
				});
				builder.show();
				return true;
			}
		});
		return true;
	}
}
