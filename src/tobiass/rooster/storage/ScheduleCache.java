package tobiass.rooster.storage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.JSONException;

import tobiass.rooster.api.Schedule;
import tobiass.rooster.misc.Const;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ScheduleCache {
	private DB db;
	private SQLiteDatabase sqlite;
	private Context context;
	public ScheduleCache(Context context) {
		db = new DB(context);
		this.context = context;
		sqlite = db.getWritableDatabase();
	}
	
	private static String[] columns = new String[] {DB.CID, DB.CFRIEND_ID, DB.CCHANGES, DB.CEXPIRY, DB.CLASTMOD, DB.CFILENAME};
	public static String tempFilePrefix = "schedule";
	public synchronized Schedule getSchedule(int friendId, boolean changes, boolean overrideExpiry) throws JSONException, IOException {
		String query = DB.CFRIEND_ID+"="+friendId+" and "+DB.CCHANGES+"="+(changes ? 1 : 0);
		if(!overrideExpiry) {
			query += " and "+DB.CEXPIRY+">"+Math.floor(System.currentTimeMillis()/1000);
		}
		Cursor c = sqlite.query(DB.TABLE_NAME_SCHEDULECACHE, columns, query, null, null, null, null, "1");		
		if(c.getCount() > 0) {
			c.moveToFirst();
			CachedSchedule cachedSchedule = new CachedSchedule();
			cachedSchedule.id = c.getInt(0);
			cachedSchedule.friendId = c.getInt(1);
			cachedSchedule.changes = c.getInt(2)>0;
			cachedSchedule.expiry = c.getInt(3);
			cachedSchedule.lastMod = c.getString(4);
			cachedSchedule.filename = c.getString(5);
			Log.d(Const.TAG, "Cached found!");
			c.close();
			return cachedSchedule.schedule(context);
		}
		c.close();
		return null;
	}
	
	public synchronized void insert(Schedule s) throws IOException, JSONException {
		ContentValues cv = s.getContentValues();
		File f = File.createTempFile(tempFilePrefix, null, context.getCacheDir());
		cv.put(DB.CFILENAME, f.getName());
		sqlite.insert(DB.TABLE_NAME_SCHEDULECACHE, null, cv);
		FileOutputStream fos = new FileOutputStream(f);
		fos.write(s.serialize().toString().getBytes());
		fos.close();
		
		removeExpired();
	}
	
	private static String[] removeExpiredColumns = new String[] {DB.CFILENAME};
	public synchronized void removeExpired() {
		String query = "expiry<"+Math.floor(System.currentTimeMillis()/1000);
		Cursor c = sqlite.query(DB.TABLE_NAME_SCHEDULECACHE, removeExpiredColumns, query, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast()) {
			new File(context.getCacheDir(), c.getString(0)).delete();
			c.moveToNext();
		}
		c.close();
		sqlite.delete(DB.TABLE_NAME_SCHEDULECACHE, query, null);
	}
	
	public synchronized void removeAll() {
		Cursor c = sqlite.query(DB.TABLE_NAME_SCHEDULECACHE, removeExpiredColumns, null, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast()) {
			new File(context.getCacheDir(), c.getString(0)).delete();
			c.moveToNext();
		}
		c.close();
		sqlite.delete(DB.TABLE_NAME_SCHEDULECACHE, null, null);
	}
	
	public void close() {
		db.close();
	}
}
