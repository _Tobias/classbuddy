package tobiass.rooster.storage;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.Static;
import android.util.Log;

public class WebStore {
	private HashMap<WebResource, Cacheable> cache = new HashMap<WebResource, WebStore.Cacheable>();
	
	public void get(final String url, final String postData, final Converter converter, final Callback callback, final int expiry, final boolean newThread) {
		get(url, postData, converter, callback, expiry, newThread, null);
	}
	
	public synchronized void get(final String url, final String postData, final Converter converter, final Callback callback, final int expiry, final boolean newThread, final String charset) {
		Iterator<WebResource> iteratorKeys = cache.keySet().iterator();
		Iterator<Cacheable> iteratorValues = cache.values().iterator();
		while(iteratorKeys.hasNext()) {
			final WebResource w = iteratorKeys.next();
			final Cacheable c = iteratorValues.next();
			
			if(c.expiry == 0 || c.expiry > System.currentTimeMillis()) {
				if((w.url == null || w.url.equals(url)) && (w.postData == null || w.postData.equals(postData)) && (w.converter == converter || w.converter.equals(converter))) {
					callback.callback(c, null, true);
					return;
				}
			}
			else {
				cache.remove(w);
				continue;
			}
		}
		
		iteratorKeys = cache.keySet().iterator();
		iteratorValues = cache.values().iterator();
		while(iteratorKeys.hasNext()) {
			final WebResource w = iteratorKeys.next();
			final Cacheable c = iteratorValues.next();
			
			if((w.url == null || w.url.equals(url)) && (w.postData == null || w.postData.equals(postData))) {
				Cacheable nc = new Cacheable(c.expiry, c.data, converter.convert(c.data));
				cache.put(new WebResource(w.url, w.postData, converter), nc);
				callback.callback(nc, null, true);
				return;
			}
		}
		
		Runnable r = new Runnable() {
			public void run() {
				Cacheable c = null;
				try {
					Log.d(Const.TAG, "WebStore: "+url);
					HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
					final boolean post = postData != null;
					conn.setRequestMethod(post ? "POST" : "GET");
					conn.setDoOutput(post);
					if(post) {
						final OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
						osw.write(postData);
						osw.flush();
					}
					String output = Static.convertStream(conn.getInputStream(), charset);
					if(converter != null) {
						c = new Cacheable(expiry, output, converter.convert(output));
					}
					else {
						c = new Cacheable(expiry, output, null);
					}
					cache.put(new WebResource(url, postData, converter), c);
				} catch (Exception e) {
					callback.callback(null, e, false);
					return;
				}
				callback.callback(c, null, false);
			}
		};
		
		if(newThread) {
			new Thread(r).start();
		}
		else {
			r.run();
		}
	}
	
	public void get(final String url, final String postData, final Converter converter, final Callback callback, final int expiry) {
		get(url, postData, converter, callback, expiry, true);
	}
	
	private class WebResource {
		String url;
		String postData;
		Converter converter;
		public WebResource(String url, String postData, Converter converter) {
			this.url = url;
			this.postData = postData;
			this.converter = converter;
		}
	}
	public class Cacheable {
		public final long expiry;
		public final String data;
		public final Object converted;
		public Cacheable(long expiry, String data, Object converted) {
			if(expiry == 0) {
				this.expiry = 0;
			}
			else {
				this.expiry = System.currentTimeMillis() + expiry*1000;
			}
			this.data = data;
			this.converted = converted;
		}
	}
	
	public interface Converter {
		public Object convert(String input);
	}
	
	public interface Callback {
		public void callback(Cacheable data, Exception e, boolean mainThread);
	}
	
	private static WebStore instance = null;
	public static WebStore getInstance() {
		if(instance == null) {
			instance = new WebStore();
		}
		return instance;
	}
}
