package tobiass.rooster.storage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tobiass.rooster.api.Friend;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.Static;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Friends {
	private DB friends;
	private SQLiteDatabase sqlite;
	public Friends(Context context) {
		friends = new DB(context);
		sqlite = friends.getWritableDatabase();
	}
	
	private static String[] columns = new String[] { DB.CID,
			DB.CTYPE,
			DB.CCLASS, DB.CABBREVIATION, DB.CDEPARTMENT,
			DB.CNAME, DB.CSTUDENT_ID, DB.CSCHOOL };
	
	private static Comparator<Friend> friendComparator = new Comparator<Friend>() {
		public int compare(Friend lhs, Friend rhs) {
			return lhs.toString().compareToIgnoreCase(rhs.toString());
		}
	};
	
	public List<Friend> getFriends() {
		List<Friend> persons = new ArrayList<Friend>();
		Cursor cursor = sqlite.query(DB.TABLE_NAME_FRIENDS, columns, null, null, null, null, DB.CNAME);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			int type = cursor.getInt(1);
			if((type & Const.TYPE_ME) == 0) {
				persons.add(cursorToFriend(cursor));
			}
			cursor.moveToNext();
		}
		cursor.close();
		Collections.sort(persons, friendComparator);

		return persons;
	}
	
	private static Friend cursorToFriend(Cursor c) {
		Friend p = new Friend();
		p.id = c.getInt(0);
		p.type = c.getInt(1);
		// unset me flag
		p.type &= ~Const.TYPE_ME;
		p._class = c.getString(2);
		p.abbreviation = c.getString(3);
		p.department = c.getString(4);
		p.name = c.getString(5);
		p.student_id = c.getInt(6);
		p.school = c.getInt(7);
		return p;
	}
	
	public Friend getMe() {
		Cursor cursor = sqlite.query(DB.TABLE_NAME_FRIENDS, columns, "("+DB.CTYPE+"&"+Const.TYPE_ME+")>0", null, null, null, DB.CNAME);
		if(cursor.getCount() == 0) {
			Log.d("friends", "me not found");
			cursor.close();
			return null;
		}
		cursor.moveToFirst();
		Friend me = null;
		me = cursorToFriend(cursor);
		cursor.close();
		return me;
	}
	
	public Friend getFriend(int id) {
		Cursor cursor = sqlite.query(DB.TABLE_NAME_FRIENDS, columns, DB.CID+"="+id, null, null, null, null, "1");
		if(cursor.getCount() == 0) {
			cursor.close();
			return null;
		}
		cursor.moveToFirst();
		Friend me = null;
		me = cursorToFriend(cursor);
		cursor.close();
		return me;
	}
	
	public void setMe(Friend f) {
		f.type |= Const.TYPE_ME;
		ContentValues c = f.getContentValues();
		if(sqlite.update(DB.TABLE_NAME_FRIENDS, c, "("+DB.CTYPE+"&"+Const.TYPE_ME+")>0", null) == 0)
			sqlite.insert(DB.TABLE_NAME_FRIENDS, null, c);
	}
	
	public void addFriend(Friend f) {
		sqlite.insert(DB.TABLE_NAME_FRIENDS, null, f.getContentValues());
	}
	
	public void updateFriend(int id, Friend f) {
		sqlite.update(DB.TABLE_NAME_FRIENDS, f.getContentValues(), "id="+id, null);
	}
	
	public void removeFriend(int id) {
		sqlite.delete(DB.TABLE_NAME_FRIENDS, "id="+id, null);
	}
	
	public void removeFriends(Friend[] friends) {
		String[] where = new String[friends.length];
		int i = 0;
		for(Friend f : friends) {
			where[i] = DB.CID+"="+f.id;
			i++;
		}
		sqlite.delete(DB.TABLE_NAME_FRIENDS, Static.join(where, " or "), null);
	}
	
	public void removeDuplicate(Friend f) {
		if(f.type == Const.TYPE_STUDENT) {
			sqlite.delete(DB.TABLE_NAME_FRIENDS, String.format("%s = "+f.student_id+" and %s = ? and %s = "+f.school,
					DB.CSTUDENT_ID, DB.CDEPARTMENT, DB.CSCHOOL), new String[] {f.department});
		}
	}
	
	private static String[] teacherFields = new String[] {DB.CABBREVIATION, DB.CTYPE, DB.CSCHOOL};
	private static String[] studentFields = new String[] {DB.CSTUDENT_ID, DB.CDEPARTMENT, DB.CTYPE, DB.CSCHOOL};
	private static String[] classFields = new String[] {DB.CNAME};
	public boolean isDuplicate(Friend f) {
		Cursor c;
		boolean r;
		switch(f.type) {
		case Const.TYPE_STUDENT:
			c = sqlite.query(false, DB.TABLE_NAME_FRIENDS, studentFields, String.format("%s="+f.student_id+" and %s=? and (%s&"+f.type+")>0 and %s="+f.school, (Object[])studentFields), new String[]{f.department}, null, null, null, "1");
			r = c.getCount() > 0;
			c.close();
			return r;
		case Const.TYPE_TEACHER:
			c = sqlite.query(false, DB.TABLE_NAME_FRIENDS, teacherFields, String.format("%s=? and (%s&"+f.type+")>0 and %s="+f.school, (Object[])teacherFields), new String[]{f.abbreviation}, null, null, null, "1");
			r = c.getCount() > 0;
			c.close();
			return r;
		}
		return false;
	}
	
	public boolean isDuplicate(String name) {
		if(name == null) {
			return false;
		}
		Cursor c = sqlite.query(false, DB.TABLE_NAME_FRIENDS, classFields, DB.CNAME+"=?", new String[]{name}, null, null, null, "1");
		boolean r = c.getCount() > 0;
		c.close();
		return r;
	}
	
	public void close() {
		friends.close();
	}
}
