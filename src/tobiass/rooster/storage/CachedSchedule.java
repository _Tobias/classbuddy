package tobiass.rooster.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.json.JSONException;

import tobiass.rooster.api.Schedule;
import tobiass.rooster.misc.Static;
import android.content.Context;

public class CachedSchedule {
	public int friendId;
	public int expiry;
	public boolean changes;
	public int id;
	public String lastMod;
	public String filename;
	
	public Schedule schedule(Context context) throws JSONException, FileNotFoundException {
		FileInputStream fis = new FileInputStream(new File(context.getCacheDir(), filename));
		Schedule s = new Schedule(Static.convertStream(fis));
		s.lastMod = lastMod;
		Friends f = new Friends(context);
		s.friend = f.getFriend(friendId);
		f.close();
		s.changes = changes;
		return s;
	}
}
