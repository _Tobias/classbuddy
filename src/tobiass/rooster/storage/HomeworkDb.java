package tobiass.rooster.storage;

import java.util.HashSet;
import java.util.Set;

import tobiass.rooster.homework.HomeworkEntry;
import tobiass.rooster.misc.Static;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class HomeworkDb {
	private DB db;
	private SQLiteDatabase sqlite;
	
	public HomeworkDb(Context c) {
		db = new DB(c);
		sqlite = db.getWritableDatabase();
	}
	
	private static HomeworkEntry cursorToHwE(Cursor c) {
		HomeworkEntry entry = new HomeworkEntry();
		entry.id = c.getInt(0);
		entry.deadline = c.getInt(1);
		entry.description = c.getString(2);
		entry.subject = c.getString(3);
		entry.priority = c.getInt(4) > 0;
		return entry;
	}
	
	// columns: CID, CDEADLINE, CDESCRIPTION, CSUBJECT, CPRIORITY
	public HomeworkEntry[] getHomework() {
		Cursor c = sqlite.query(DB.TABLE_NAME_HOMEWORK, null, String.format("%s>=%d", DB.CDEADLINE, Static.getNeutralTimestamp()), null, null, null, DB.CDEADLINE);
		HomeworkEntry[] entries = new HomeworkEntry[c.getCount()];
		c.moveToFirst();
		int pointer = 0;
		while(!c.isAfterLast()) {
			entries[pointer] = cursorToHwE(c);
			c.moveToNext();
			pointer++;
		}
		c.close();
		return entries;
	}
	
	public void addHomework(HomeworkEntry entry) {
		sqlite.insert(DB.TABLE_NAME_HOMEWORK, null, entry.getContentValues());
	}
	
	public void updateHomework(HomeworkEntry entry) {
		sqlite.update(DB.TABLE_NAME_HOMEWORK, entry.getContentValues(), String.format("%s=%d", DB.CID, entry.id), null);
	}
	
	public void cleanup() {
		sqlite.delete(DB.TABLE_NAME_HOMEWORK, String.format("%s<%d", DB.CDEADLINE, Static.getNeutralTimestamp()), null);
	}
	
	public Set<Integer> getDays() {
		Cursor c = sqlite.query(true, DB.TABLE_NAME_HOMEWORK, new String[] {DB.CDEADLINE}, null, null, null, null, null, null);
		HashSet<Integer> list = new HashSet<Integer>();
		c.moveToFirst();
		while(!c.isAfterLast()) {
			list.add(c.getInt(0));
			c.moveToNext();
		}
		c.close();
		return list;
	}
	
	public HomeworkEntry[] getHomeworkOnDay(int day) {
		Cursor c = sqlite.query(DB.TABLE_NAME_HOMEWORK, null, String.format("%s=%d", DB.CDEADLINE, day), null, null, null, DB.CPRIORITY+" DESC, "+DB.CSUBJECT);
		HomeworkEntry[] entries = new HomeworkEntry[c.getCount()];
		c.moveToFirst();
		int pointer = 0;
		while(!c.isAfterLast()) {
			entries[pointer] = cursorToHwE(c);
			pointer++;
			c.moveToNext();
		}
		c.close();
		return entries;
	}
	
	public void removeHomework(int id) {
		sqlite.delete(DB.TABLE_NAME_HOMEWORK, DB.CID+"="+id, null);
	}
	
	public String[] getSubjects() {
		Cursor c = sqlite.query(DB.TABLE_NAME_SUBJECTS, null, null, null, null, null, null);
		String[] entries = new String[c.getCount()];
		c.moveToFirst();
		int pointer = 0;
		while(!c.isAfterLast()) {
			entries[pointer] = c.getString(1);
			pointer++;
			c.moveToNext();
		}
		return entries;
	}
	
	public void addSubject(String name) {
		ContentValues cv = new ContentValues(1);
		cv.put(DB.CNAME, name);
		sqlite.insert(DB.TABLE_NAME_SUBJECTS, null, cv);
	}
	
	public void removeSubject(String name) {
		sqlite.delete(DB.TABLE_NAME_SUBJECTS, DB.CNAME+"=?", new String[]{name});
	}
	
	public void close() {
		db.close();
	}
}
