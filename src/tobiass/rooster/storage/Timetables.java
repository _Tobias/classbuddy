package tobiass.rooster.storage;

import java.util.Iterator;

import tobiass.rooster.R;
import tobiass.rooster.timetable.Timetable;
import tobiass.rooster.timetable.TimetableLesson;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Timetables {
	private DB db;
	private SQLiteDatabase sqlite;
	public Timetables(Context context) {
		db = new DB(context);
		sqlite = db.getWritableDatabase();
		initPresets(context);
	}
	
	public static void initPresets(Context context) {
		if(PRESET_NAMES != null)
			return;
		PRESET_NAMES = new String[] {context.getString(R.string.timetable_preset_1), context.getString(R.string.timetable_preset_2)};
		int length = 50;
		PRESETS[0] = new Timetable();
		PRESETS[0].add(false, (byte) 8, (byte) 20, length);
		PRESETS[0].add(false, (byte) 9, (byte) 10, length);
		PRESETS[0].add(false, (byte) 10, (byte) 0, length);
		PRESETS[0].add(true, (byte) 10, (byte) 50, 20);
		PRESETS[0].add(false, (byte) 11, (byte) 10, length);
		PRESETS[0].add(false, (byte) 12, (byte) 0, length);
		PRESETS[0].add(true, (byte) 12, (byte) 50, 30);
		PRESETS[0].add(false, (byte) 13, (byte) 20, length);
		PRESETS[0].add(false, (byte) 14, (byte) 10, length);
		PRESETS[0].add(true, (byte) 15, (byte) 0, 10);
		PRESETS[0].add(false, (byte) 15, (byte) 10, length);
		PRESETS[1] = new Timetable();
		PRESETS[1].add(false, (byte) 8, (byte) 20, length);
		PRESETS[1].add(false, (byte) 9, (byte) 10, length);
		PRESETS[1].add(true, (byte) 10, (byte) 0, 20);
		PRESETS[1].add(false, (byte) 10, (byte) 20, length);
		PRESETS[1].add(false, (byte) 11, (byte) 10, length);
		PRESETS[1].add(true, (byte) 12, (byte) 0, 30);
		PRESETS[1].add(false, (byte) 12, (byte) 30, length);
		PRESETS[1].add(false, (byte) 13, (byte) 20, length);
		PRESETS[1].add(false, (byte) 14, (byte) 10, length);
		PRESETS[1].add(true, (byte) 15, (byte) 0, 10);
		PRESETS[1].add(false, (byte) 15, (byte) 10, length);
	}
	
	public void insertTimetable(Timetable table, String preset) {
		Iterator<TimetableLesson> i = table.iterator();
		while(i.hasNext()) {
			ContentValues v = i.next().getContentValues();
			v.put(DB.CPRESET, preset);
			sqlite.insert(DB.TABLE_NAME_TIMETABLE, null, v);
		}
	}
	
	private static String[] PRESET_NAMES;
	public static final Timetable[] PRESETS = new Timetable[] {null, null};
	
	public boolean timetableExists(String preset) {
		for(int i = 0; i < PRESET_NAMES.length; i++) {
			if(PRESET_NAMES[i].equals(preset)) {
				return true;
			}
		}
		
		Cursor cursor = sqlite.query(DB.TABLE_NAME_TIMETABLE, null, DB.CPRESET+"=?", new String[]{preset}, null, null, null, "1");
		int num = cursor.getCount();
		cursor.close();
		return num > 0;
	}
	
	public Timetable getTimetable(String preset) {
		for(int i = 0; i < PRESET_NAMES.length; i++) {
			if(PRESET_NAMES[i].equals(preset)) {
				return PRESETS[i];
			}
		}
		
		Timetable table = new Timetable();
		Cursor cursor = sqlite.query(DB.TABLE_NAME_TIMETABLE, null, DB.CPRESET+"=?", new String[]{preset}, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			table.add(new TimetableLesson(cursor.getInt(1) > 0, (byte) cursor.getInt(2), (byte) cursor.getInt(3), cursor.getInt(4)));
			cursor.moveToNext();
		}
		cursor.close();
		return table.size() > 0 ? table : null;
	}
	
	public String[] getPresetNames() {
		Cursor c = sqlite.query(true, DB.TABLE_NAME_TIMETABLE, new String[] {DB.CPRESET}, null, null, null, null, null, null);
		String[] result = new String[c.getCount()+PRESET_NAMES.length];
		int pointer = 0;
		for(String presetName : PRESET_NAMES) {
			result[pointer] = presetName;
			pointer++;
		}
		c.moveToFirst();
		while(!c.isAfterLast()) {
			result[pointer] = c.getString(0);
			c.moveToNext();
			pointer++;
		}
		c.close();
		return result;
	}
	
	public boolean isSysPreset(String preset) {
		for(String s : PRESET_NAMES) {
			if(preset.equals(s))
				return true;
		}
		return false;
	}
	
	public void delete(String preset) {
		sqlite.delete(DB.TABLE_NAME_TIMETABLE, DB.CPRESET+"=?", new String[]{preset});
	}
	
	public void close() {
		db.close();
	}
}
