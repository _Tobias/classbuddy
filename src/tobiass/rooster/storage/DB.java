package tobiass.rooster.storage;

import java.io.File;

import tobiass.rooster.misc.Migrate;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DB extends SQLiteOpenHelper {
	protected static final String DATABASE_NAME = "persist.db";
	protected static final int DATABASE_VERSION = 7;
	public static final String TABLE_NAME_FRIENDS = "friends";
	public static final String TABLE_NAME_SCHEDULECACHE = "schedule_cache";
	public static final String TABLE_NAME_TIMETABLE = "timetable";
	public static final String TABLE_NAME_HOMEWORK = "homework";
	public static final String TABLE_NAME_SUBJECTS = "subjects";

	public static final String CID = "id";
	
	public static final String CTYPE = "type";
	public static final String CCLASS = "class";
	public static final String CNAME = "name";
	public static final String CDEPARTMENT = "department";
	public static final String CSTUDENT_ID = "student_id";
	public static final String CABBREVIATION = "abbreviation";
	public static final String CSCHOOL = "school";
	
	public static final String CFRIEND_ID = "friend_id";
	public static final String CCHANGES = "changes";
	public static final String CEXPIRY = "expiry";
	public static final String CFILENAME = "filename";
	public static final String CLASTMOD = "lastmod";
	
	public static final String CPRESET = "preset";
	public static final String CIS_BREAK = "is_break";
	public static final String CSTARTHOUR = "starthour";
	public static final String CSTARTMINUTE = "startmin";
	public static final String CLENGTH = "length";
	
	public static final String CDEADLINE = "deadline";
	public static final String CDESCRIPTION = "description";
	public static final String CSUBJECT = "subject";
	public static final String CPRIORITY = "priority";
	
	protected static final String CREATE_TABLE_FRIENDS = String
			.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TINYINT, %s VARCHAR(50), %s VARCHAR(12), %s VARCHAR(12), %s SMALLINT, %s VARCHAR(5), %s SMALLINT);",
					TABLE_NAME_FRIENDS, CID, CTYPE, CNAME, CCLASS, CDEPARTMENT, CSTUDENT_ID,
					CABBREVIATION, CSCHOOL);
	
	protected static final String CREATE_TABLE_SCHEDULECACHE = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s BOOLEAN, %s INTEGER, %s VARCHAR(30), %s VARCHAR(30));",
			TABLE_NAME_SCHEDULECACHE, CID, CFRIEND_ID, CCHANGES, CEXPIRY, CLASTMOD, CFILENAME);
	
	protected static final String CREATE_TABLE_TIMETABLE = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s BOOLEAN, %s INTEGER, %s INTEGER, %s INTEGER, %s TEXT);",
			TABLE_NAME_TIMETABLE, CID, CIS_BREAK, CSTARTHOUR, CSTARTMINUTE, CLENGTH, CPRESET);
	
	protected static final String CREATE_TABLE_HOMEWORK = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER);",
			TABLE_NAME_HOMEWORK, CID, CDEADLINE, CDESCRIPTION, CSUBJECT, CPRIORITY);
	
	protected static final String CREATE_TABLE_SUBJECTS = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT);",
			TABLE_NAME_SUBJECTS, CID, CNAME);

	private Context context = null;
	public DB(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}
	
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_FRIENDS);
		db.execSQL(CREATE_TABLE_SCHEDULECACHE);
		db.execSQL(CREATE_TABLE_TIMETABLE);
		db.execSQL(CREATE_TABLE_HOMEWORK);
		db.execSQL(CREATE_TABLE_SUBJECTS);
		
		// migrate from the old textfile versions
		Migrate.database(db, context);
	}
	
	public void delete() {
		close();
		context.deleteDatabase(DATABASE_NAME);
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion < 2) {
			db.execSQL(CREATE_TABLE_SCHEDULECACHE);
		}
		if(oldVersion < 3) {
			// delete all cache because of new mappings
			File f = context.getCacheDir();
			String[] list = f.list();
			for(String file : list) {
				if(file.startsWith(ScheduleCache.tempFilePrefix)) {
					new File(f, file).delete();
				}
			}
			db.delete(TABLE_NAME_SCHEDULECACHE, null, null);
		}
		if(oldVersion < 4)
			db.execSQL(CREATE_TABLE_TIMETABLE);
		if(oldVersion < 5)
			db.execSQL(CREATE_TABLE_HOMEWORK);
		else if(oldVersion < 6) { // add priority field, 'else if' because the table that is recreated is newly created in the previous statement
			db.execSQL("DROP TABLE "+TABLE_NAME_HOMEWORK);
			db.execSQL(CREATE_TABLE_HOMEWORK);
		}
		if(oldVersion < 7)
			db.execSQL(CREATE_TABLE_SUBJECTS);
	}
}