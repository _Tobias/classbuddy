package tobiass.rooster;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class UpdatableAdapter extends BaseAdapter {
	private List<String> mData;
	private Context mContext;
	public UpdatableAdapter(Context context, List<String> initialData) {
		mData = initialData != null ? initialData : new ArrayList<String>();
		mContext = context;
	}
	
	public int getCount() {
		return mData.size();
	}
	
	public void updateData(List<String> newData) {
		mData = newData;
		notifyDataSetChanged();
	}

	public Object getItem(int position) {
		return mData.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		TextView t = (TextView) (convertView == null ? View.inflate(mContext, android.R.layout.simple_list_item_1, null) : convertView);
		t.setText(mData.get(position));
		return t;
	}
}
