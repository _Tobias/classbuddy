package tobiass.rooster.fragments;

import tobiass.rooster.FirstRun;
import tobiass.rooster.R;
import tobiass.rooster.Viewer;
import tobiass.rooster.api.Friend;
import tobiass.rooster.background.NotificationService;
import tobiass.rooster.background.NotificationService.ServiceBinder;
import tobiass.rooster.homework.Homework;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.Static;
import tobiass.rooster.storage.Friends;
import tobiass.rooster.storage.Timetables;
import tobiass.rooster.timetable.TimetableEditor;
import tobiass.widget.TFragment;
import tobiass.widget.TGridView;
import tobiass.widget.TGridView.OnSwitchListener;
import tobiass.widget.TileAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Toast;

public class PersonalFragment extends TFragment {
	protected static String TAG = "PersonalFragment";
	private TileAdapter adapter;
	private int shortcutType;
	
	private void noTimetable() {
		adapter.setChecked(3, false);
		PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putBoolean(Const.PREF_CLASSGUIDE, false).commit();
		if(service != null)
			service.updateSettings();
		startActivity(new Intent(getActivity(), TimetableEditor.class));
		Toast.makeText(getActivity(), R.string.timetable_intro, Toast.LENGTH_LONG).show();
	}
	
	public View onSCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frag_personal, null);
		TGridView grid = (TGridView) view.findViewById(R.id.grid);
		
		final Activity activity = getActivity();
		final SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
		Friends f = new Friends(activity);
		final Friend me = f.getMe();
		f.close();
		
		SparseBooleanArray checked = new SparseBooleanArray(2);
		checked.put(2, prefs.getBoolean(Const.PREF_NOTIFICATIONS, false));
		checked.put(3, prefs.getBoolean(Const.PREF_CLASSGUIDE, false));
		
		adapter = new TileAdapter(activity, checked, R.array.tile_names, R.array.tile_icons, R.array.tile_type_switch, R.layout.frag_personal_tile, R.id.image, R.id.text, R.id.checkbox);
		grid.setAdapter(adapter);
		
		grid.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				switch(arg2) {
				case 0:
				case 1://schedule/changes
					Viewer.start(activity, me, false, arg2 == 1);
					break;
					
				case 2: //notifications
				case 3: //classguide
					adapter.toggle(arg2);
					break;
					
				case 4:
					startActivity(new Intent(activity, TimetableEditor.class));
					break;
					
				case 5:
					startActivity(new Intent(activity, Homework.class));
					break;
					
				case 6: //setup profile
					startActivity(new Intent(activity, FirstRun.class).putExtra(Const.SHOW_HOME, true));
					break;
					
				case 7:// rate
					Static.openPlayStore(activity);
					break;
					
				case 8: //share
					Intent intent = new Intent(android.content.Intent.ACTION_SEND);
					intent.setType("text/plain");
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

					// Add data to the intent, the receiving app will decide what to do with it.
					intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
					intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_message)+"\nhttp://play.google.com/store/apps/details?id="+activity.getPackageName());
					startActivity(Intent.createChooser(intent, getString(R.string.share_app)));
					break;
				case 9: //share myself
					me.share(activity);
					break;
				}
			}
		});
		
		final AlertDialog shortcutConfirmation = new AlertDialog.Builder(activity)
		.setMessage(R.string.shortcut_creation_confirmation)
		.setNegativeButton(R.string.cancel, null)
		.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				createIntentShortcut(shortcutType == 0 ? Const.SCHEDULE : Const.SCHEDULE_CHANGES, shortcutType == 0 ? getString(R.string.schedule) : getString(R.string.changes));
				Toast.makeText(activity, R.string.shortcut_created, Toast.LENGTH_SHORT).show();
			}
		})
		.create();
		
		grid.setOnItemLongClickListener(new OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				switch(arg2) {
				case 0:
				case 1:
					shortcutType = arg2;
					shortcutConfirmation.show();
					return true;
				}
				return false;
			}
		});
		
		grid.setOnSwitchListener(new OnSwitchListener() {			
			public void onSwitch(int which, boolean checked) {
				if(which == 2)
					prefs.edit().putBoolean(Const.PREF_NOTIFICATIONS, checked).commit();
					
				else if(which == 3) {
					if(checked) {
						String timetable = prefs.getString(Const.PREF_TIMETABLE, null);
						if(timetable != null) {
							Timetables t = new Timetables(activity);
							if(!t.timetableExists(timetable))
								noTimetable();
							else
								prefs.edit().putBoolean(Const.PREF_CLASSGUIDE, checked).commit();
							
							t.close();
						}
						else
							noTimetable();
					}
					else
						prefs.edit().putBoolean(Const.PREF_CLASSGUIDE, false).commit();
				}
				
				if(service != null)
					service.updateSettings();
				
				if(checked && service == null) {
					activity.bindService(serviceIntent, connection, 0);
					activity.startService(serviceIntent);
				}
			}
		});
		
		// check if the selected timetable exists
		if(checked.get(3)) {
			String timetable = prefs.getString(Const.PREF_TIMETABLE, null);
			if(timetable != null) {
				Timetables t = new Timetables(activity);
				if(!t.timetableExists(timetable))
					noTimetable();
				
				t.close();
			}
			else
				noTimetable();
		}
		
		return view;
	}
	
	public boolean makeScrollable() {
		return false;
	}
	public boolean usePadding() {
		return false;
	}
	
	private void createIntentShortcut(String intent, String title) {
		Context context = getActivity().getApplicationContext();
		context.sendBroadcast(new Intent()
		.putExtra(Intent.EXTRA_SHORTCUT_INTENT, new Intent(context, Viewer.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra(intent, true))
		.putExtra(Intent.EXTRA_SHORTCUT_NAME, title)
		.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(context, R.drawable.ic_launcher))
		.setAction("com.android.launcher.action.INSTALL_SHORTCUT"));
	}
	
	public NotificationService service;
	private ServiceConnection connection = new ServiceConnection() {
		public void onServiceDisconnected(ComponentName arg0) {
			service = null;
		}
		
		public void onServiceConnected(ComponentName arg0, IBinder arg1) {
			service = ((ServiceBinder)arg1).getService();
		}
	};
	
	private Friend me;
	private Intent serviceIntent;
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Activity activity = getActivity();
		
		serviceIntent = new Intent(activity, NotificationService.class);
		
		activity.bindService(serviceIntent, connection, 0);
		activity.startService(serviceIntent);
		
		Friends f = new Friends(activity);
		me = f.getMe();
		f.close();
		if(me == null) {
			activity.finish();
			activity.startActivity(new Intent(activity, FirstRun.class));
			return;
		}
	}
	
	public void onDestroy() {
		if(adapter != null) {
			adapter.cleanup();
		}
		getActivity().unbindService(connection);
		super.onDestroy();
	}
}