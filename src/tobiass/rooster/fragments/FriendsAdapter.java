package tobiass.rooster.fragments;

import java.util.Iterator;
import java.util.List;

import tobiass.rooster.R;
import tobiass.rooster.api.Friend;
import tobiass.rooster.misc.Const;
import android.content.Context;
import android.support.v4.app.ListFragment;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FriendsAdapter extends ArrayAdapter<Friend> implements OnItemLongClickListener, OnItemClickListener, FriendsInterfaces.Callback {
	protected static String TAG = "FriendsAdapter";
	private LayoutInflater inf;
	private final static int layout = R.layout.row_friend;
	private SparseBooleanArray checked;
	private SparseArray<View> views;
	private FriendsInterfaces.Controller controller;
	private final static int checkedBackground = R.color.highlight;
	
	public FriendsAdapter(Context context, List<Friend> objects, FriendsInterfaces.Controller controller) {
		super(context, layout, objects);
		inf = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		checked = new SparseBooleanArray(objects.size());
		views = new SparseArray<View>(objects.size());
		
		this.controller = controller;
	}
	
	public View getView(int pos, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder h = null;
		if(v == null) {
			v = inf.inflate(layout, null);
			h = new ViewHolder();
			h.textView = (TextView) v.findViewById(android.R.id.text1);
			h.details = (TextView) v.findViewById(R.id.details);
			h.imageView = (ImageView) v.findViewById(R.id.selection_indicator);
			h.pos = pos;
			v.setTag(h);
			views.put(pos, v);
		}
		else {
			h = (ViewHolder) v.getTag();
			views.remove(h.pos);
			views.put(pos, v);
			h.pos = pos;
		}
		boolean _checked = checked.get(pos);
		h.imageView.setVisibility(_checked ? View.VISIBLE : View.GONE);
		v.setBackgroundResource(_checked ? checkedBackground : 0);
		Friend f = getItem(pos);
		h.textView.setText(f.toString());
		switch(f.type) {
		case Const.TYPE_CLASS:
			h.details.setText(f._class);
			break;
		case Const.TYPE_STUDENT:
			h.details.setText(String.valueOf(f.student_id));
			break;
		default:
			h.details.setText(null);
		}
		return v;
	}
	
	private static class ViewHolder {
		public TextView textView;
		public TextView details;
		public ImageView imageView;
		public int pos;
	}
	
	public int getCheckedCount() {
		int count = 0;
		for(int i = 0; i < checked.size(); i++) {
			if(checked.get(checked.keyAt(i))) {
				count++;
			}
		}
		return count;
	}
	
	public Friend[] getCheckedItems() {
		int count = getCheckedCount();
		Friend[] friends = new Friend[count];
		int pointer = 0;
		// loop through all checked-elements
		for(int i = 0; i < checked.size(); i++) {
			int key = checked.keyAt(i);
			if(checked.get(key)) {
				friends[pointer] = getItem(key);
				pointer++;
			}
		}
		return friends;
	}
	
	public Friend getFirstCheckedItem() {
		for(int i = 0; i < checked.size(); i++) {
			int key = checked.keyAt(i);
			if(checked.get(key)) {
				return getItem(key);
			}
		}
		return null;
	}
	
	public void toggleChecked(int position) {
		setChecked(position, !checked.get(position));
	}
	
	public void setData(List<Friend> list) {
		if(getCheckedCount() > 0) {
			checked.clear();
			controller.getActionMode().finish();
		}
		clear();
		views.clear();
		Iterator<Friend> i = list.iterator();
		while(i.hasNext()) {
			add(i.next());
		}
	}
	
	public void setChecked(int position, boolean checked) {
		int before = getCheckedCount();
		this.checked.put(position, checked);
		View v = views.get(position);
		if(v != null) {
			v.setBackgroundResource(checked ? checkedBackground : 0);
			ViewHolder h = (ViewHolder) v.getTag();
			h.imageView.setVisibility(checked ? View.VISIBLE : View.GONE);
		}
		int after = getCheckedCount();
		controller.setCount(after);
		if((before == 1 && after > 1) || (before > 1 && after == 1)) {
			controller.getActionMode().invalidate();
		}
		else if(before == 0 && after == 1) {
			controller.startActionMode();
		}
		else if(before > 0 && after == 0) {
			controller.getActionMode().finish();
		}
	}
	
	public void setAllChecked(boolean checked) {
		int count = 0;
		int total = getCount();
		for(int i = 0; i < total; i++) {
			if(this.checked.get(i))
				count++;
			this.checked.put(i, checked);
			View v = views.get(i);
			if(v != null) {
				v.setBackgroundResource(checked ? checkedBackground : 0);
				ViewHolder h = (ViewHolder) v.getTag();;
				h.imageView.setVisibility(checked ? View.VISIBLE : View.GONE);
			}
		}
		controller.setCount(checked ? total : 0);
		if(checked && count == 0 && total > 0) {
			controller.startActionMode();
		}
		else if(!checked && count > 0) {
			if(controller.getActionMode() != null) {
				controller.getActionMode().finish();
			}
		}
		else {
			if(controller.getActionMode() != null) {
				controller.getActionMode().invalidate();
			}
		}
	}

	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if(getCheckedCount() > 0)
			toggleChecked(arg2);
		else
			controller.itemClicked(getItem(arg2));
	}

	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		toggleChecked(arg2);
		return true;
	}
	
	public static FriendsAdapter attach(ListFragment fragment, List<Friend> objects, FriendsInterfaces.Controller controller) {
		FriendsAdapter adapter = new FriendsAdapter(fragment.getActivity(), objects, controller);
		fragment.setListAdapter(adapter);
		fragment.getListView().setOnItemClickListener(adapter);
		fragment.getListView().setOnItemLongClickListener(adapter);
		return adapter;
	}
	
	public static FriendsAdapter attach(ListView listView, List<Friend> objects, FriendsInterfaces.Controller controller) {
		FriendsAdapter adapter = new FriendsAdapter(listView.getContext(), objects, controller);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(adapter);
		listView.setOnItemLongClickListener(adapter);
		return adapter;
	}

	public void actionModeFinished() {
		setAllChecked(false);
		checked.clear();
		controller.setCount(0);
	}
	
	public void removeCheckedItems() {
		Friend[] friends = getCheckedItems();
		if(friends.length == 0) {
			return;
		}
		checked.clear();
		controller.setCount(0);
		for(Friend f : friends) {
			int pos = getPosition(f);
			// shift the views
			for(int i = pos; i < getCount(); i++) {
				views.put(i, views.get(i+1));
				views.delete(i+1);
			}
			remove(f);
		}
		controller.getActionMode().finish();
	}
}
