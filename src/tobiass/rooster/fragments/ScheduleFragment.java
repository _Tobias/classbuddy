package tobiass.rooster.fragments;

import tobiass.rooster.Main;
import tobiass.rooster.R;
import tobiass.rooster.TestDrive;
import tobiass.rooster.UpdatableAdapter;
import tobiass.rooster.Viewer;
import tobiass.rooster.api.Friend;
import tobiass.rooster.api.School;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.ErrorDismissWatcher;
import tobiass.rooster.splash.SomeFries;
import tobiass.rooster.storage.Friends;
import tobiass.widget.TActionProcessButton;
import tobiass.widget.TFragment;
import tobiass.widget.TRadioGroup;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class ScheduleFragment extends TFragment {
	private EditText student_id;
	private TRadioGroup schedule_type;
	private Spinner school;
	private Spinner department;
	private Spinner _class;
	private Spinner teacher;
	private Spinner classroom;
	private Button schedule;
	private TActionProcessButton save_friend;
	private ViewFlipper schedule_flipper;
	private SparseArray<String> schools;
	private School school_object;
	
	private boolean spinnersInitialized;
	
	private String toSetError;
	private Runnable setStudentError = new Runnable() {
		public void run() {
			student_id.setError(toSetError);
		}
	};
	private void setStudentError(String text) {
		toSetError = text;
		getActivity().runOnUiThread(setStudentError);
	}
	
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(!isVisibleToUser && student_id != null) {
	    	student_id.setError(null);
	    }
	}
	
	public View onSCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final Main activity = (Main) getActivity();
		View view = inflater.inflate(R.layout.frag_schedule, null);
		
		// input views
		student_id = (EditText) view.findViewById(R.id.student_id);
		schedule_type = new TRadioGroup((RadioGroup) view.findViewById(R.id.schedule_type_pt1), (RadioGroup) view.findViewById(R.id.schedule_type_pt2));
		school = (Spinner) view.findViewById(R.id.school_spinner);
		department = (Spinner) view.findViewById(R.id.department_spinner);
		department.setAdapter(new UpdatableAdapter(activity, null));
		_class = (Spinner) view.findViewById(R.id.class_spinner);
		_class.setAdapter(new UpdatableAdapter(activity, null));
		teacher = (Spinner) view.findViewById(R.id.teacher_spinner);
		teacher.setAdapter(new UpdatableAdapter(activity, null));
		classroom = (Spinner) view.findViewById(R.id.classroom_spinner);
		classroom.setAdapter(new UpdatableAdapter(activity, null));
		
		schedule_flipper = (ViewFlipper) view.findViewById(R.id.schedule_flipper);
		
		save_friend = new TActionProcessButton(view.findViewById(R.id.save_friend));		
		
		schedule = (Button) view.findViewById(R.id.schedule);
		
		student_id.addTextChangedListener(new ErrorDismissWatcher(student_id));
		
		schedule.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {				
				if(student_id.getText().toString().equals("00000")) {
					startActivity(new Intent(getActivity(), SomeFries.class));
					return;
				}
				else if(student_id.getText().toString().equals("00001")) {
					startActivity(new Intent(getActivity(), TestDrive.class));
					return;
				}
				
				Friend f = new Friend();
				if(fillFriend(f)) {
					Viewer.start(getActivity(), f, false, true);
				}
			}
		});
		
		save_friend.getButton().setOnClickListener(new OnClickListener() {
			private Friend f = null;
			private Runnable saveFriend = new Runnable() {
				public void run() {
					if(f != null) {
						Friends fh = new Friends(activity);
						fh.addFriend(f);
						activity.refreshFriends(fh);
						fh.close();
						Toast.makeText(activity, R.string.friend_saved, Toast.LENGTH_SHORT).show();
					}
					
					setLocked(false);
					setLoading(false);
					save_friend.setProgress(false);
				}
			};
			private View nameAskDialogView = activity.getLayoutInflater().inflate(R.layout.dialog_nameask, null);
			private EditText nameAskField;
			private AlertDialog nameAskDialog = new AlertDialog.Builder(activity)
			.setView(nameAskDialogView)
			.setPositiveButton(R.string.save_friend, null)
			.setNegativeButton(R.string.cancel, null)
			.setOnCancelListener(new OnCancelListener() {
						public void onCancel(DialogInterface dialog) {
							if(nameAskField == null) {
								nameAskField = (EditText) nameAskDialogView.findViewById(R.id.input);
								nameAskField.addTextChangedListener(new ErrorDismissWatcher(nameAskField));
							}
							nameAskField.setText(null);
						}
					})
			.create();
			
			public void onClick(View v) {
				f = new Friend();
				if(!fillFriend(f)) {
					return;
				}
				Friends fr = new Friends(activity);
				if(fr.isDuplicate(f)) {
					Toast.makeText(activity, fr.getMe().equals(f) ? R.string.error_friend_me : R.string.friend_duplicate, Toast.LENGTH_SHORT).show();
					fr.close();
					return;
				}
				
				fr.close();
				if(f.type == Const.TYPE_STUDENT) {					
					setLocked(true);
					save_friend.setProgress(true);
					if(!save_friend.isProcessButton()) {
						setLoading(true);
					}
					
					new Thread(new Runnable() {
						public void run() {
							String error = f.addName(activity);
							if(error != null) {
								setStudentError(error);
								f = null;
							}
							activity.runOnUiThread(saveFriend);
						}
					}).start();
				}
				else if(f.type == Const.TYPE_CLASS) {
					nameAskDialog.show();
					nameAskDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							if(nameAskField == null) {
								nameAskField = (EditText) nameAskDialogView.findViewById(R.id.input);
								nameAskField.addTextChangedListener(new ErrorDismissWatcher(nameAskField));
							}
							String name = nameAskField.getText().toString().trim();
							if(name.length() == 0) {
								nameAskField.setError(getText(R.string.name_too_short));
								return;
							}
							Friends fh = new Friends(activity);
							boolean dupe = fh.isDuplicate(name);
							fh.close();
							if(dupe) {
								nameAskField.setError(getText(R.string.friend_duplicate));
							}
							else {
								f.name = name;
								saveFriend.run();
								nameAskDialog.cancel();
							}
						}
					});
					nameAskDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							nameAskDialog.cancel();
						}
					});
				}
				else if(f.type == Const.TYPE_TEACHER) {
					saveFriend.run();
				}
			}
		});
		
		schedule_type.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				save_friend.getButton().setVisibility(checkedId == R.id.type_classroom ? View.GONE : View.VISIBLE);
				int a = 0;
				switch(checkedId) {
				case R.id.type_student:
					a = 0;
					break;
				case R.id.type_class:
					a = 1;
					break;
				case R.id.type_teacher:
					a = 2;
					break;
				case R.id.type_classroom:
					a = 3;
					break;
				}
				schedule_flipper.setDisplayedChild(a);
			}
		});
		
		school.setOnItemSelectedListener(new OnItemSelectedListener() {
			private Runnable setupSpinners = new Runnable() {
				public void run() {
					setLocked(false);
					setLoading(false);
					if(school_object == null) {
						Toast.makeText(activity, getText(R.string.error_no_inet), Toast.LENGTH_SHORT).show();
						return;
					}
					((UpdatableAdapter) department.getAdapter()).updateData(school_object.getDepartments());
					((UpdatableAdapter) _class.getAdapter()).updateData(school_object.getClasses());
					((UpdatableAdapter) classroom.getAdapter()).updateData(school_object.getClassrooms());
					((UpdatableAdapter) teacher.getAdapter()).updateData(school_object.getTeachers());
				}
			};
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				final int school_id = schools.keyAt(arg2);
				if(school_id == school_object.id) {
					Bundle args = getArguments();
					if(args != null && args.size() > 0) {
						department.setSelection(args.getInt(Const.DEPARTMENT));
						_class.setSelection(args.getInt(Const.CLASS));
						teacher.setSelection(args.getInt(Const.ABBREVIATION));
						classroom.setSelection(args.getInt(Const.CLASSROOM));
						args.clear();
					}
					return;
				}
				// fill spinners
				if(!School.hasCached(school_id)) {
					// thread
					setLocked(true);
					setLoading(true);
					new Thread(new Runnable() {
						public void run() {
							school_object = School.getSchool(activity, school_id);
							activity.runOnUiThread(setupSpinners);
						}
					}).start();
				}
				else {
					school_object = School.getSchool(activity, school_id);
					setupSpinners.run();
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) {}
		});
		
		return view;
	}
	
	private boolean fillFriend(Friend req) {
		req.school = school_object.id;
		req.type = getReqType();
		switch(req.type) {
		case Const.TYPE_STUDENT:
			int student_id_val = 0;
			try {
				student_id_val = Integer.parseInt(student_id.getText().toString());
			}
			catch(NumberFormatException ex) {
				setStudentError(getString(R.string.invalid_number));
				return false;
			}
			req.student_id = student_id_val;
			req.department = (String) department.getSelectedItem();
			break;
		case Const.TYPE_CLASS:
			req._class = (String) _class.getSelectedItem();
			break;
		case Const.TYPE_CLASSROOM:
			req.classroom = (String) classroom.getSelectedItem();
			break;
		case Const.TYPE_TEACHER:
			req.abbreviation = (String) teacher.getSelectedItem();
			break;
		}
		
		return true;
	}
	
	private int getReqType() {
		switch(schedule_type.getCheckedId()) {
		case R.id.type_student:
			return Const.TYPE_STUDENT;
		case R.id.type_class:
			return Const.TYPE_CLASS;
		case R.id.type_classroom:
			return Const.TYPE_CLASSROOM;
		case R.id.type_teacher:
			return Const.TYPE_TEACHER;
		}
		return 0;
	}
	
	public void onDestroyView() {
		spinnersInitialized = false;
		super.onDestroyView();
	}
	
	public void onResume() {
		super.onResume();
		if(!spinnersInitialized) { // && visible
		    final Activity activity = getActivity();
		    
		    Friends f = new Friends(activity);
			final int school_id = f.getMe().school;
			f.close();
			
		    final Runnable setupSpinners = new Runnable() {
				public void run() {
					setLocked(false);
					setLoading(false);
					
					if(schools == null || school_object == null) {
						Toast.makeText(activity, getText(R.string.error_no_inet), Toast.LENGTH_SHORT).show();
						return;
					}
					
					school.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, getArrayFromSparse(schools)));
					((UpdatableAdapter) department.getAdapter()).updateData(school_object.getDepartments());
					((UpdatableAdapter) _class.getAdapter()).updateData(school_object.getClasses());
					((UpdatableAdapter) classroom.getAdapter()).updateData(school_object.getClassrooms());
					((UpdatableAdapter) teacher.getAdapter()).updateData(school_object.getTeachers());
					spinnersInitialized = true;
					
					Bundle args = getArguments();
					if(args != null && args.size() > 0) {						
						schedule_type.check(args.getInt(Const.TYPE));
						school.setSelection(args.getInt(Const.SCHOOL));
						student_id.setText(args.getString(Const.STUDENT_ID));
					}
					else {
						school.setSelection(schools.indexOfKey(school_id));
					}
				}
			};
			
			if(!(School.hasCached(school_id) && School.hasSchoolListCached())) {
				// thread
				setLocked(true);
				setLoading(true);
				new Thread(new Runnable() {
					public void run() {
						schools = School.getList(activity);
						school_object = School.getSchool(activity, school_id);
						activity.runOnUiThread(setupSpinners);
					}
				}).start();
			}
			else {
				schools = School.getList(activity);
				school_object = School.getSchool(activity, school_id);
				setupSpinners.run();
			}
		}
	}
	
	public void onSaveInstanceState(Bundle out) {
		out.putString(Const.STUDENT_ID, student_id.getText().toString());
		out.putInt(Const.TYPE, schedule_type.getCheckedId());
		out.putInt(Const.DEPARTMENT, department.getSelectedItemPosition());
		out.putInt(Const.CLASS, _class.getSelectedItemPosition());
		out.putInt(Const.ABBREVIATION, teacher.getSelectedItemPosition());
		out.putInt(Const.CLASSROOM, classroom.getSelectedItemPosition());
		out.putInt(Const.SCHOOL, school.getSelectedItemPosition());
	}
	
	private static String[] getArrayFromSparse(SparseArray<String> s) {
		String[] a = new String[s.size()];
		for(int i = 0; i < s.size(); i++) {
			a[i] = s.get(s.keyAt(i));
		}
		return a;
	}
}