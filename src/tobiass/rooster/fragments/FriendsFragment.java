package tobiass.rooster.fragments;

import java.util.List;

import tobiass.rooster.Main;
import tobiass.rooster.R;
import tobiass.rooster.Viewer;
import tobiass.rooster.api.Friend;
import tobiass.rooster.misc.ErrorDismissWatcher;
import tobiass.rooster.storage.Friends;
import tobiass.widget.TListFragment;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class FriendsFragment extends TListFragment implements FriendsInterfaces.Controller {	
	private ActionMode mMode;
	
	private ListView listView;
	
	private Main activity;
	private FriendsAdapter adapter;
	
	public boolean usePadding() {
		return false;
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		activity = (Main) getActivity();
		
		View noFriends = getLayoutInflater(null).inflate(R.layout.view_no_friends, null);
		listView = getListView();
		((ViewGroup) getView()).addView(noFriends);
		listView.setEmptyView(noFriends);
		refresh();
	}
	
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(!isVisibleToUser && mMode != null) {
	    	mMode.finish();
	    	adapter.actionModeFinished();
	    }
	}
	
	public void refresh() {
		Friends f = new Friends(getActivity());
		refresh(f);
		f.close();
	}
	
	public void refresh(Friends f) {
		if(listView == null) {
			return;
		}
		if(mMode != null) {
			mMode.finish();
			adapter.actionModeFinished();
		}
		List<Friend> friends = f.getFriends();
		if(adapter != null) {
			adapter.setData(friends);
		}
		else {
			adapter = FriendsAdapter.attach(this, friends, this);
		}
	}
	
	private class FriendManager implements ActionMode.Callback {
		private MenuItem delete;
		private MenuItem rename;
		private MenuItem share;
		private MenuItem selectAll;
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			rename = menu.add(R.string.rename).setIcon(R.drawable.ic_action_edit);
			rename.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			
			share = menu.add(R.string.share).setIcon(R.drawable.ic_action_share);
			share.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			
			selectAll = menu.add(R.string.select_all).setIcon(R.drawable.ic_action_select_all);
			selectAll.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			
			delete = menu.add(R.string.delete).setIcon(R.drawable.ic_action_discard);
			delete.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			return true;
		}

		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			if(checkedCount > 1) {
				if(!rename.isVisible() && !rename.isEnabled()) {
					return false;
				}
				else {
					rename.setVisible(false);
					rename.setEnabled(false);
				}
			}
			else if(checkedCount == 1) {
				if(rename.isVisible() && rename.isEnabled()) {
					return false;
				}
				else {
					rename.setVisible(true);
					rename.setEnabled(true);
				}
			}
			else {
				return false;
			}
			return true;
		}
		
		private AlertDialog deleteConfirmation = new AlertDialog.Builder(getActivity())
		.setPositiveButton(R.string.yes, new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Friends f = new Friends(getActivity());
				f.removeFriends(adapter.getCheckedItems());
				adapter.removeCheckedItems();
				f.close();
				dialog.dismiss();
			}
		})
		.setNegativeButton(R.string.cancel, new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		})
		.create();
		
		private View renameView = getActivity().getLayoutInflater().inflate(R.layout.dialog_namechange, null);
		private EditText renameField = null;
		private Friend toUpdate = null;
		private AlertDialog renameDialog = new AlertDialog.Builder(getActivity())
		.setPositiveButton(R.string.save_friend, null)
		.setNegativeButton(R.string.cancel, null)
		.setView(renameView)
		.create();
		
		private View.OnClickListener renameDialogPositiveButton = new View.OnClickListener() {
			public void onClick(View v) {
				Friends f = new Friends(getActivity());
				String newName = renameField.getText().toString();
				if(newName.length() == 0) {
					renameField.setError(getText(R.string.name_too_short));
					f.close();
					return;
				}
				if(toUpdate.name != null && toUpdate.name.equals(newName)) {
					f.close();
					renameDialog.cancel();
					return;
				}
				
				if(f.isDuplicate(newName)) {
					renameField.setError(getText(R.string.friend_duplicate));
					f.close();
					return;
				}
				
				toUpdate.name = newName;
				f.updateFriend(toUpdate.id, toUpdate);
				refresh(f);
				f.close();
				renameDialog.cancel();
			}
		};
		
		private View.OnClickListener renameDialogNegativeButton = new View.OnClickListener() {
			public void onClick(View v) {
				renameDialog.cancel();
			}
		};

		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			if(item == delete) {
				if(checkedCount == 1) {
					//deleteConfirmation.setTitle(R.string.delete_friend);
					deleteConfirmation.setMessage(getString(R.string.delete_confirmation));
					deleteConfirmation.show();
				}
				else if(checkedCount > 1) {
					//deleteConfirmation.setTitle(R.string.delete_friends);
					deleteConfirmation.setMessage(getString(R.string.delete_confirmation_multiple));
					deleteConfirmation.show();
				}
			}
			else if(item == rename) {				
				toUpdate = adapter.getFirstCheckedItem();
				
				if(renameField == null) {
					renameField = (EditText) renameView.findViewById(R.id.input);
					renameField.addTextChangedListener(new ErrorDismissWatcher(renameField));
				}
				renameField.setText(toUpdate.name);
				
				// setup buttons after show
				
				renameDialog.show();
				
				renameDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(renameDialogNegativeButton);
				renameDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(renameDialogPositiveButton);
			}
			else if(item == selectAll) {
				adapter.setAllChecked(true);
			}
			else if(item == share) {
				Friend[] friends = adapter.getCheckedItems();
				if(friends != null) {
					Friend.share(activity, friends);
				}
			}
            return true;
		}

		public void onDestroyActionMode(ActionMode mode) {
			mMode = null;
			adapter.actionModeFinished();
		}
	}

	public ActionMode getActionMode() {
		return mMode;
	}

	public void startActionMode() {
		mMode = activity.startActionMode(new FriendManager());
	}

	private int checkedCount = 0;
	public void setCount(int count) {
		checkedCount = count;
	}

	public void itemClicked(Friend friend) {
		Viewer.start(activity, friend, false, true);
	}
}