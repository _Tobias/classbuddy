package tobiass.rooster.fragments;

import tobiass.rooster.api.Friend;

import com.actionbarsherlock.view.ActionMode;

public class FriendsInterfaces {
	public interface Controller {
		public ActionMode getActionMode();
		public void startActionMode();
		public void setCount(int count);
		public void itemClicked(Friend friend);
	}
	
	public interface Callback {
		public void actionModeFinished();
	}
}
