package tobiass.rooster.homework;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;

import org.json.JSONException;

import tobiass.rooster.R;
import tobiass.rooster.Viewer;
import tobiass.rooster.api.Day;
import tobiass.rooster.api.Friend;
import tobiass.rooster.api.Hour;
import tobiass.rooster.api.Lesson;
import tobiass.rooster.api.Schedule;
import tobiass.rooster.api.Week;
import tobiass.rooster.homework.SortedAdapter.OnEmptyChangedListener;
import tobiass.rooster.homework.SortedAdapter.OnRemoveListener;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.ErrorDismissWatcher;
import tobiass.rooster.misc.Static;
import tobiass.rooster.storage.Friends;
import tobiass.rooster.storage.HomeworkDb;
import tobiass.rooster.storage.ObjectStore;
import tobiass.rooster.storage.ScheduleCache;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;

public class HomeworkSubjects extends SherlockActivity implements OnRemoveListener, OnItemClickListener, OnEmptyChangedListener {
	private ViewSwitcher mSwitcher;
	private EditText mName;
	private ListView mListView;
	private SortedAdapter mAdapter;
	private HomeworkDb mHomeworkDb;
	private static String NAME = "a";
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_homework_subjects);

		mSwitcher = (ViewSwitcher) findViewById(R.id.switcher);
		mListView = (ListView) findViewById(R.id.list);
		mListView.setOnItemClickListener(this);
		mName = (EditText) findViewById(R.id.subject_name);
		if(savedInstanceState != null)
			mName.setText(savedInstanceState.getString(NAME));
		mName.addTextChangedListener(new ErrorDismissWatcher(mName));
	}
	
	public void onSaveInstanceState(Bundle out) {
		out.putString(NAME, mName.getText().toString());
	}
	
	public void onResume() {
		super.onResume();
		if(mHomeworkDb == null)
			mHomeworkDb = new HomeworkDb(this);
		
		mAdapter = new SortedAdapter(this, mHomeworkDb.getSubjects());
		mAdapter.setOnRemoveListener(this);
		mAdapter.setOnEmptyChangedListener(this);
		mAdapter.callOnEmptyListener();
		mListView.setAdapter(mAdapter);
	}
	
	public void onPause() {
		if(mHomeworkDb != null) {
			mHomeworkDb.close();
			mHomeworkDb = null;
		}
		super.onPause();
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			onBackPressed();
		}
		return true;
	}
	
	public void addSubject(View v) {
		String newText = mName.getText().toString().trim();
		if(newText.length() == 0) {
			mName.setError(getText(R.string.name_too_short));
			return;
		}
		else if(mAdapter.contains(newText)) {
			mName.setError(getText(R.string.name_duplicate));
			return;
		}
		
		mHomeworkDb.addSubject(newText);
		mAdapter.add(newText);
		
		mName.setText(null);
	}

	private String toastShowing = null;
	private Toast toast;
	public void onRemove(String subject) {
		if(toastShowing == null) {
			toastShowing = subject;
			toast = Toast.makeText(this, getString(R.string.tap_again_to_remove, subject), Toast.LENGTH_SHORT);
			toast.show();
		}
		else if(toastShowing.equals(subject) && toast.getView().isShown()) {
			mAdapter.remove(subject);
			mHomeworkDb.removeSubject(subject);
			toastShowing = null;
			toast.cancel();
		}
		else { // they don't match
			toastShowing = subject;
			toast.cancel();
			toast = Toast.makeText(this, getString(R.string.tap_again_to_remove, subject), Toast.LENGTH_SHORT);
			toast.show();
		}
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(R.string.import_from_schedule).setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				ProgressDialog dialog = ProgressDialog.show(HomeworkSubjects.this, null, getText(R.string.just_a_sec));
				Friends f = new Friends(HomeworkSubjects.this);
				Friend me = f.getMe();
				f.close();
				
				ScheduleCache cache = new ScheduleCache(HomeworkSubjects.this);
				Schedule s = null;
				try {
					s = cache.getSchedule(me.id, false, true);
				}
				catch (JSONException e) {}
				catch (IOException e) {}
				cache.close();
				
				if(s == null)
					startActivityForResult(Viewer.startIntent(HomeworkSubjects.this, me, false, false, true), 0);
				else
					importSchedule(s);
				
				dialog.cancel();
				return true;
			}
		}).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(getIntent().getBooleanExtra(Const.RETURN_SUBJECT, false)) {
			setResult(RESULT_OK, getIntent().putExtra(Const.SUBJECT, mAdapter.get(position)));
			finish();
		}
	}

	public void onEmptyChanged(boolean empty) {
		mSwitcher.setDisplayedChild(empty ? 1 : 0);
	}
	
	public void onActivityResult(int request, int response, Intent data) {
		if(response == RESULT_OK)
			importSchedule((Schedule) ObjectStore.get(Viewer.OSI, data.getIntExtra(Const.SCHEDULE, -1)));
	}
	
	private void importSchedule(Schedule s) {
		HashSet<String> set = new HashSet<String>();
		
		Week w = s.schedule;
		Iterator<Day> di = w.iterator();
		while(di.hasNext()) {
			Iterator<Hour> hi = di.next().iterator();
			while(hi.hasNext()) {
				Iterator<Lesson> li = hi.next().iterator();
				while(li.hasNext()) {
					String sub = li.next().subject;
					if(sub != null && sub.length() > 0)
						set.add(sub);
				}
			}
		}
		
		final String[] entries = set.toArray(new String[set.size()]);
		
		new AlertDialog.Builder(this)
		.setMessage(getString(R.string.subject_import_confirmation, Static.join(entries, ", ")))
		.setPositiveButton(R.string.yes, new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				for(String s : entries) {
					if(!mAdapter.contains(s)) {
						mHomeworkDb.addSubject(s);
						mAdapter.addNoUpdate(s);
					}
				}
				mAdapter.update();
			}
		})
		.setNegativeButton(R.string.cancel, null)
		.show();
	}
}
