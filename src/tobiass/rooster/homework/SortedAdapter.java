package tobiass.rooster.homework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tobiass.rooster.R;
import tobiass.rooster.misc.Static;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SortedAdapter extends BaseAdapter implements OnClickListener {
	private Context mContext;
	private List<String> mSubjects;
	public SortedAdapter(Context context, String[] entries) {
		mContext = context;
		mSubjects = new ArrayList<String>(entries.length);
		for(String s : entries)
			mSubjects.add(s);
		Collections.sort(mSubjects, Static.stringComparator);
	}
	
	public void callOnEmptyListener() {
		if(mOnEmptyChangedListener != null)
			mOnEmptyChangedListener.onEmptyChanged(mSubjects.size() == 0);
	}
	
	public void add(String s) {
		if(mSubjects.size() == 0 && mOnEmptyChangedListener != null)
			mOnEmptyChangedListener.onEmptyChanged(false);
		mSubjects.add(s);
		Collections.sort(mSubjects, Static.stringComparator);
		notifyDataSetChanged();
	}
	
	public void addNoUpdate(String s) {
		mSubjects.add(s);
	}
	
	public void update() {
		callOnEmptyListener();
		Collections.sort(mSubjects, Static.stringComparator);
		notifyDataSetChanged();
	}
	
	public void addAll(String[] entries) {
		if(mSubjects.size() == 0 && mOnEmptyChangedListener != null)
			mOnEmptyChangedListener.onEmptyChanged(false);
		
		for(String s : entries)
			mSubjects.add(s);
		Collections.sort(mSubjects, Static.stringComparator);
		notifyDataSetChanged();
	}
	
	public void remove(String s) {
		mSubjects.remove(s);
		notifyDataSetChanged();
		
		if(mSubjects.size() == 0 && mOnEmptyChangedListener != null)
			mOnEmptyChangedListener.onEmptyChanged(true);
	}
	
	public String get(int position) {
		return mSubjects.get(position);
	}
	
	public boolean contains(String s) {
		return mSubjects.contains(s);
	}
	
	public int getCount() {
		return mSubjects.size();
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewGroup v = (ViewGroup) convertView;
		TextView text;
		ImageView button;
		if(v == null) {
			v = (ViewGroup) View.inflate(mContext, R.layout.row_removable, null);
			text = (TextView) v.findViewById(android.R.id.text1);
			button = (ImageView) v.findViewById(R.id.remove);
			v.setTag(new View[] {text, button});
		}
		else {
			View[] views = (View[]) v.getTag();
			text = (TextView) views[0];
			button = (ImageView) views[1];
		}
		
		button.setOnClickListener(this);
		button.setTag(position);
		
		text.setText(mSubjects.get(position));
		return v;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}
	
	private OnEmptyChangedListener mOnEmptyChangedListener;
	private OnRemoveListener mOnRemoveListener;
	public void setOnEmptyChangedListener(OnEmptyChangedListener arg) {
		mOnEmptyChangedListener = arg;
	}
	public void setOnRemoveListener(OnRemoveListener arg) {
		mOnRemoveListener = arg;
	}
	
	public interface OnRemoveListener {
		public void onRemove(String subject);
	}
	
	public interface OnEmptyChangedListener {
		public void onEmptyChanged(boolean empty);
	}

	public void onClick(View v) {
		if(mOnRemoveListener != null) {
			mOnRemoveListener.onRemove(mSubjects.get((Integer) v.getTag()));
		}
	}
}
