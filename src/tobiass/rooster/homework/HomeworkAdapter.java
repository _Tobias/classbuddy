package tobiass.rooster.homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import tobiass.rooster.R;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.Static;
import tobiass.rooster.storage.HomeworkDb;
import tobiass.rooster.storage.ObjectStore;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class HomeworkAdapter extends BaseAdapter implements OnClickListener, DialogInterface.OnClickListener {
	private Context mContext;
	private HomeworkDb mProvider;
	private Integer[] mDays;
	
	public HomeworkAdapter(Context context, HomeworkDb provider) {
		mContext = context;
		mProvider = provider;
		Set<Integer> set = mProvider.getDays();
		mDays = set.toArray(new Integer[set.size()]);
		Arrays.sort(mDays);
	}
	
	public void callOnEmptyListener() {
		if(mOnEmptyChangedListener != null)
			mOnEmptyChangedListener.onEmptyChanged(mDays.length == 0);
	}
	
	public void update() {
		Set<Integer> set = mProvider.getDays();
		mDays = set.toArray(new Integer[set.size()]);
		Arrays.sort(mDays);
		callOnEmptyListener();
		notifyDataSetChanged();
	}
	
	public int getCount() {
		return mDays.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}
	
	private List<ViewGroup> itemViews = new ArrayList<ViewGroup>();
	private ViewGroup getItemView() {
		if(itemViews.size() > 0) {
			ViewGroup item = itemViews.get(0);
			itemViews.remove(0);
			return item;
		}
		else {
			ViewGroup item = (ViewGroup) View.inflate(mContext, R.layout.row_homework_entry, null);
			ItemViewHolder holder = new ItemViewHolder();
			holder.subject = (TextView) item.findViewById(R.id.subject);
			holder.description = (TextView) item.findViewById(R.id.description);
			item.setOnClickListener(this);
			item.setTag(holder);
			return item;
		}
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewGroup v = (ViewGroup) convertView;
		DayViewHolder dayViewHolder = null;
		if(v == null) {
			v = (ViewGroup) View.inflate(mContext, R.layout.row_homework_day_overview, null);
			dayViewHolder = new DayViewHolder();
			
			dayViewHolder.dayDate = (TextView) v.findViewById(R.id.day_date);
			dayViewHolder.itemCounter = (TextView) v.findViewById(R.id.item_counter);
			
			v.setTag(dayViewHolder);
		}
		else {
			while(v.getChildCount() > 1) {
				int index = v.getChildCount()-1;
				itemViews.add((ViewGroup) v.getChildAt(index));
				v.removeViewAt(index);
			}
			dayViewHolder = (DayViewHolder) v.getTag();
		}
		
		final Integer day = mDays[position];
		
<<<<<<< HEAD
		HomeworkEntry[] homework = mProvider.getHomeworkOnDay(day);
		
		dayViewHolder.dayDate.setText(Static.stampToString(day));
		dayViewHolder.itemCounter.setText(String.valueOf(homework.length));
=======
		HomeworkEntry[] deadlines = mProvider.getHomeworkOnDay(day, true);
		HomeworkEntry[] finishOns = mProvider.getHomeworkOnDay(day, false);
		
		dayViewHolder.dayDate.setText(Static.stampToString(day));
		dayViewHolder.itemCounter.setText(String.valueOf(deadlines.length + finishOns.length));
		
		for(HomeworkEntry hw : deadlines) {
			ViewGroup item = getItemView();
			ItemViewHolder holder = (ItemViewHolder) item.getTag();
			holder.typeIndicator.setText("D");
			if(hw.priority)
				holder.typeIndicator.append("!");
			
			item.setBackgroundResource(hw.priority ? R.drawable.background_homework_entry_urgent : R.drawable.background_homework_entry);
			holder.subject.setText(hw.subject);
			holder.description.setText(hw.description);
			holder.representing = hw;
			v.addView(item);
		}
>>>>>>> Some homework stuff updated
		
		for(HomeworkEntry hw : homework) {
			ViewGroup item = getItemView();
			ItemViewHolder holder = (ItemViewHolder) item.getTag();
<<<<<<< HEAD
			
			item.setBackgroundResource(hw.priority ? R.drawable.background_homework_entry_urgent : R.drawable.background_homework_entry);
=======
			holder.typeIndicator.setText("F");
			if(hw.priority)
				holder.typeIndicator.append("!");
			
			item.setBackgroundResource(hw.priority ? R.drawable.background_homework_entry_urgent_finish : R.drawable.background_homework_entry_finish);
>>>>>>> Some homework stuff updated
			holder.subject.setText(hw.subject);
			holder.description.setText(hw.description);
			holder.representing = hw;
			v.addView(item);
		}
		
		return v;
	}
	
	private static class DayViewHolder {
		public TextView dayDate;
		public TextView itemCounter;
	}
	
	private static class ItemViewHolder {
		public HomeworkEntry representing;
		public TextView subject;
		public TextView description;
	}

	public void onClick(View v) {
		HomeworkEntry entry = ((ItemViewHolder) v.getTag()).representing;
		CharSequence[] options = mContext.getResources().getTextArray(R.array.homework_entry_options);
		options[2] = mContext.getResources().getText(entry.priority ? R.string.homework_entry_set_not_important : R.string.homework_entry_set_important);
		
		new AlertDialog.Builder(mContext)
		.setItems(options, this)
		.show().getListView().setTag(entry);
	}
	
	public void onClick(DialogInterface dialog, int which) {
		HomeworkEntry entry = (HomeworkEntry) ((AlertDialog) dialog).getListView().getTag();
		switch(which) {
		case 0: // delete
			mProvider.removeHomework(entry.id);
			update();
			break;
		case 1: // edit
			mContext.startActivity(new Intent(mContext, HomeworkAdd.class).putExtra(Const.EDIT_HOMEWORK, ObjectStore.store(HomeworkAdd.OSI, entry)));
			break;
		case 2: // toggle priority
			entry.priority = !entry.priority;
			mProvider.updateHomework(entry);
			update();
			break;
		case 3: // share			
			Static.shareText(mContext, mContext.getText(R.string.homework)+" "+entry.subject+"\n"+entry.description+"\n"+Static.stampToString(entry.deadline), null);
			break;
		}
	}
	
	public interface OnEmptyChangedListener {
		public void onEmptyChanged(boolean empty);
	}
	
	private OnEmptyChangedListener mOnEmptyChangedListener;
	public void setOnEmptyChangedListener(OnEmptyChangedListener arg) {
		mOnEmptyChangedListener = arg;
	}
}
