package tobiass.rooster.homework;

import java.text.DateFormat;
import java.util.Calendar;

import tobiass.rooster.R;
import tobiass.rooster.misc.Const;
import tobiass.rooster.misc.ErrorDismissWatcher;
import tobiass.rooster.misc.Static;
import tobiass.rooster.storage.HomeworkDb;
import tobiass.rooster.storage.ObjectStore;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;

public class HomeworkAdd extends SherlockActivity implements OnMenuItemClickListener, OnDateSetListener {
	EditText mSubject;
	EditText mDescription;
	CheckBox mPriority;
	TextView mDeadline;
	Calendar mDeadlineCal;
	HomeworkDb mHomeworkDb;
	HomeworkEntry saveTo;
	
	static final String SUBJECT = "a";
	static final String DESCRIPTION = "b";
	static final String PRIORITY = "c";
	static final String DEADLINE = "d";
	
	public static final int OSI = 3;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_homework_add);
		
		mSubject = (EditText) findViewById(R.id.subject_name);
		mSubject.addTextChangedListener(new ErrorDismissWatcher(mSubject));
		mDescription = (EditText) findViewById(R.id.description);
		mPriority = (CheckBox) findViewById(R.id.priority);
		mDeadline = (TextView) findViewById(R.id.deadline);
		mDeadlineCal = Calendar.getInstance();
		
		Intent starter = getIntent();
		int pointer = starter.getIntExtra(Const.EDIT_HOMEWORK, -1);
		starter.removeExtra(Const.EDIT_HOMEWORK);
		
		if(pointer != -1) {
			saveTo = (HomeworkEntry) ObjectStore.get(OSI, pointer);
			
			mSubject.setText(saveTo.subject);
			mDescription.setText(saveTo.description);
			mPriority.setChecked(saveTo.priority);
			mDeadlineCal.setTimeInMillis((long)saveTo.deadline*1000);
		}
		else if(savedInstanceState != null) {
			pointer = savedInstanceState.getInt(Const.EDIT_HOMEWORK, -1);
			if(pointer != -1)
				saveTo = (HomeworkEntry) ObjectStore.get(OSI, pointer);
			
			mSubject.setText(savedInstanceState.getString(SUBJECT));
			mDescription.setText(savedInstanceState.getString(DESCRIPTION));
			mPriority.setChecked(savedInstanceState.getBoolean(PRIORITY));
			mDeadlineCal.setTimeInMillis((long)savedInstanceState.getInt(DEADLINE)*1000);
		}
		refreshDate();
	}
	
	public void onSaveInstanceState(Bundle b) {
		if(saveTo != null)
			b.putInt(Const.EDIT_HOMEWORK, ObjectStore.store(OSI, saveTo));
		b.putString(SUBJECT, mSubject.getText().toString());
		b.putString(DESCRIPTION, mDescription.getText().toString());
		b.putBoolean(PRIORITY, mPriority.isChecked());
		b.putInt(DEADLINE, Static.getNeutralTimestamp(mDeadlineCal));
	}
	
	public void onResume() {
		super.onResume();
		if(mHomeworkDb == null)
			mHomeworkDb = new HomeworkDb(this);
	}
	
	public void onPause() {
		if(mHomeworkDb != null) {
			mHomeworkDb.close();
			mHomeworkDb = null;
		}
		super.onPause();
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			onBackPressed();
		}
		return true;
	}
	
	public void onBackPressed() {
		if(mSubject.getText().length() > 0 || mDescription.getText().length() > 0) {
			new AlertDialog.Builder(this)
			.setMessage(R.string.discard_warning)
			.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			})
			.setNegativeButton(R.string.cancel, null)
			.show();
		}
		else
			finish();
	}
	
	public void pickDate(View v) {
		DatePickerDialog dialog = new DatePickerDialog(this, this, mDeadlineCal.get(Calendar.YEAR), mDeadlineCal.get(Calendar.MONTH), mDeadlineCal.get(Calendar.DAY_OF_MONTH));
		dialog.show();
	}
	
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		mDeadlineCal.set(year, monthOfYear, dayOfMonth);
		refreshDate();
	}
	
	private void refreshDate() {
		mDeadline.setText(DateFormat.getDateInstance().format(mDeadlineCal.getTime()));
	}
	
	public void selectSubject(View v) {
		startActivityForResult(new Intent(this, HomeworkSubjects.class).putExtra(Const.RETURN_SUBJECT, true), 0);
	}
	
	public void onActivityResult(int request, int response, Intent data) {
		if(response == RESULT_OK)
			mSubject.setText(data.getStringExtra(Const.SUBJECT));
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(R.string.save_homework)
		.setIcon(R.drawable.ic_action_accept)
		.setOnMenuItemClickListener(this)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	public boolean onMenuItemClick(MenuItem item) {
		String subject = mSubject.getText().toString().trim();
		if(subject.length() == 0) {
			mSubject.setError(getText(R.string.name_too_short));
			return true;
		}
		HomeworkEntry entry = saveTo != null ? saveTo : new HomeworkEntry();
		entry.subject = subject;
<<<<<<< HEAD
		entry.deadline = Static.getNeutralTimestamp(mDeadlineCal);
=======
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(mDeadline.getYear(), mDeadline.getMonth(), mDeadline.getDayOfMonth());
		entry.deadline = Static.getNeutralTimestamp(cal);
		if(mFinishOn.getVisibility() == View.VISIBLE) {
			cal.set(mFinishOn.getYear(), mFinishOn.getMonth(), mFinishOn.getDayOfMonth());
			entry.finishOn = Static.getNeutralTimestamp(cal);
		}
>>>>>>> Some homework stuff updated
		entry.priority = mPriority.isChecked();
		entry.description = mDescription.getText().toString().trim();
		
		if(saveTo != null)
			mHomeworkDb.updateHomework(saveTo);
		else
			mHomeworkDb.addHomework(entry);
		finish();
		return true;
	}
}
