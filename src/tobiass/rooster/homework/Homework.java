package tobiass.rooster.homework;

import tobiass.rooster.R;
import tobiass.rooster.homework.HomeworkAdapter.OnEmptyChangedListener;
import tobiass.rooster.storage.HomeworkDb;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;

public class Homework extends SherlockActivity implements OnEmptyChangedListener {
	private HomeworkDb mProvider;
	private ViewSwitcher mSwitcher;
	private ListView mList;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_homework);
		
		mSwitcher = (ViewSwitcher) findViewById(R.id.switcher);
		mList = (ListView) findViewById(R.id.list);
	    TextView actionBarTitle = (TextView) getWindow().findViewById(getResources().getIdentifier("action_bar_title", "id", "android"));
	    if(actionBarTitle == null)
	    	actionBarTitle = (TextView) getWindow().findViewById(R.id.abs__action_bar_title);
	    
	    actionBarTitle.setTextColor(Color.BLACK);
	}
	
	public void onResume() {
		super.onResume();
		if(mProvider == null)
			mProvider = new HomeworkDb(this);
		
		HomeworkAdapter adapter = new HomeworkAdapter(this, mProvider);
		adapter.setOnEmptyChangedListener(this);
		adapter.callOnEmptyListener();
		mList.setAdapter(adapter);
	}
	
	public void onPause() {
		if(mProvider != null) {
			mProvider.close();
			mProvider = null;
		}
		super.onPause();
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			onBackPressed();
		}
		return true;
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(R.string.subjects).setIcon(R.drawable.ic_subjects_ab).setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				startActivity(new Intent(Homework.this, HomeworkSubjects.class));
				return true;
			}
		}).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		menu.add(R.string.add_homework).setIcon(R.drawable.ic_action_new).setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				startActivity(new Intent(Homework.this, HomeworkAdd.class));
				return true;
			}
		}).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	public void onEmptyChanged(boolean empty) {
		mSwitcher.setDisplayedChild(empty ? 1 : 0);
	}
}
