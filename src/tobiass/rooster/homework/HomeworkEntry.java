package tobiass.rooster.homework;

import tobiass.rooster.storage.DB;
import android.content.ContentValues;

public class HomeworkEntry {
	public int deadline;
	public String description;
	public String subject;
	public boolean priority;
	public int id;
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues(4);
		cv.put(DB.CDEADLINE, deadline);
		cv.put(DB.CDESCRIPTION, description);
		cv.put(DB.CSUBJECT, subject);
		cv.put(DB.CPRIORITY, priority);
		return cv;
	}
}
