# ClassBuddy #
ClassBuddy is an app for checking school schedules. It's based on the Dutch Gepro system by Schoolmaster.

## Dependencies ##
- Android Support Library v4 (included as project)
- ActionBarSherlock
- GifMovieView
- ActionBar-PullToRefresh (depends on SmoothProgressBar)
- SmoothProgressBar
- Widgets (see repo TWidgets)